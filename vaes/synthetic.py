"""MG 12/10/2019 Generate synthetic data for trivial experiment"""
import numpy as np
import os
import argparse


parser = argparse.ArgumentParser(description='generate mixture data')
parser.add_argument('--mu1', type=float, default=-3.,
                    help='Mean of 1st distribution')
parser.add_argument('--mu2', type=float, default=4.,
                    help='Mean of 2nd distribution')
parser.add_argument('--scale1', type=float, default=1.,
                    help='Scale of 1st distribution')
parser.add_argument('--scale2', type=float, default=2.,
                    help='Scale of 2nd distribution')
parser.add_argument('--half_train', type=float, default=5000,
                    help='Half of train size')
parser.add_argument('--half_valid', type=float, default=500,
                    help='Half of validation size')
parser.add_argument('--half_test', type=float, default=500,
                    help='Half of test size')
args = parser.parse_args()

if __name__ == '__main__':
    # generate data from two normal distribs
    train = np.random.normal(args.mu1, args.scale1, args.half_train)
    valid = np.random.normal(args.mu1, args.scale1, args.half_valid)
    test = np.random.normal(args.mu1, args.scale1, args.half_test)
    train = np.concatenate((train, np.random.normal(args.mu2, args.scale2,
                                                    args.half_train)))
    valid = np.concatenate((valid, np.random.normal(args.mu2, args.scale2,
                                                    args.half_valid)))
    test = np.concatenate((test, np.random.normal(args.mu2, args.scale2,
                                                  args.half_test)))

    # shuffle to mix distributions
    np.random.shuffle(train)
    np.random.shuffle(valid)
    np.random.shuffle(test)

    # save data for later reuse
    data_path = '/home/magda/datasets/synthetic_mixture/'
    train.tofile(os.path.join(data_path, 'train.txt'), sep=" ")
    valid.tofile(os.path.join(data_path, 'valid.txt'), sep=" ")
    test.tofile(os.path.join(data_path, 'test.txt'), sep=" ")
