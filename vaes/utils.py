"""MG 29/06/2019 Tensorflow implementation of vae vampprior"""
import tensorflow as tf
import numpy as np
import os
import shutil
import time
layers = tf.layers


def load_mnist(batch_size=64, img_size=784):
    """Load Larochelle's static binarized MNIST"""
    data_path = '/home/magda/datasets/binarized_mnist/'

    # train set - shuffle and set train batch size as per param
    data = np.fromfile(os.path.join(data_path, 'binarized_mnist_train.amat'),
                       dtype='float32', sep=" ")
    data = data.reshape([-1, img_size])
    datasets = {'train': tf.data.Dataset.from_tensor_slices(data)}
    datasets['train'] = datasets['train'].shuffle(buffer_size=len(data))
    datasets['train'] = datasets['train'].batch(batch_size)

    # validation set - batch is the whole set
    data = np.fromfile(os.path.join(data_path, 'binarized_mnist_valid.amat'),
                       dtype='float32', sep=" ")
    data = data.reshape([-1, img_size])
    datasets['valid'] = tf.data.Dataset.from_tensor_slices(data)
    datasets['valid'] = datasets['valid'].batch(len(data))

    # test set - batch is the whole set
    # repeat for multiple calculations over same data
    data = np.fromfile(os.path.join(data_path, 'binarized_mnist_test.amat'),
                       dtype='float32', sep=" ")
    data = data.reshape([-1, img_size])
    datasets['test'] = tf.data.Dataset.from_tensor_slices(data)
    datasets['test'] = datasets['test'].shuffle(buffer_size=len(data))
    datasets['test'] = datasets['test'].batch(len(data))
    datasets['test'] = datasets['test'].repeat()
    # datasets['test'] = datasets['test'].batch(20)

    return datasets


def load_synthetic(batch_size=64):
    """Load Larochelle's static binarized MNIST"""
    data_path = '/home/magda/datasets/synthetic_mixture/'

    # train set - shuffle and set train batch size as per param
    data = np.fromfile(os.path.join(data_path, 'train.txt'),
                       dtype='float32', sep=" ")
    data = data.reshape([-1, 1])
    datasets = {'train': tf.data.Dataset.from_tensor_slices(data)}
    datasets['train'] = datasets['train'].shuffle(buffer_size=len(data))
    datasets['train'] = datasets['train'].batch(batch_size)

    # validation set - batch is the whole set
    data = np.fromfile(os.path.join(data_path, 'valid.txt'),
                       dtype='float32', sep=" ")
    data = data.reshape([-1, 1])
    datasets['valid'] = tf.data.Dataset.from_tensor_slices(data)
    datasets['valid'] = datasets['valid'].batch(len(data))

    # test set - batch is the whole set
    # repeat for multiple calculations over same data
    data = np.fromfile(os.path.join(data_path, 'test.txt'),
                       dtype='float32', sep=" ")
    data = data.reshape([-1, 1])
    datasets['test'] = tf.data.Dataset.from_tensor_slices(data)
    datasets['test'] = datasets['test'].shuffle(buffer_size=len(data))
    datasets['test'] = datasets['test'].batch(len(data))
    datasets['test'] = datasets['test'].repeat()
    # datasets['test'] = datasets['test'].batch(20)

    return datasets


def make_write_folders(args):
    model_name = ('BS' + str(args.batch_size) +
                  'LR' + str(args.learning_rate) +
                  'NN' + str(args.number_of_nodes) +
                  'ZS' + str(args.z_size))
    if args.run_id != '':
        model_name = model_name + 'ID' + args.run_id
    if args.model == 'vampprior':
        model_name = model_name + 'NU' + str(args.num_pseudo)
    tb_path = os.path.join('./', args.experiment_name, args.tb_folder,
                           args.model, model_name)
    saver_path = os.path.join('./', args.experiment_name, args.saver_folder,
                              args.model, model_name)
    time_str = time.strftime("%Y%m%d-%H%M%S")
    print_path = os.path.join('./', args.experiment_name, args.print_folder,
                              args.model, model_name, time_str)
    if not args.restore:
        try:
            shutil.rmtree(tb_path)
        except os.error:
            os.makedirs(tb_path)
        try:
            shutil.rmtree(saver_path)
        except os.error:
            os.makedirs(saver_path)
    os.makedirs(print_path)  # always new cause time_str

    pathes = tb_path, saver_path, print_path

    return pathes


def layer_gated_dense(x, number_of_nodes, name):
    with tf.variable_scope(name):
        f = layers.dense(x,
                         units=number_of_nodes,
                         use_bias=True,
                         kernel_initializer=tf.glorot_normal_initializer,
                         name='f')
        g = layers.dense(x,
                         units=number_of_nodes,
                         use_bias=True,
                         activation=tf.sigmoid,
                         kernel_initializer=tf.glorot_normal_initializer,
                         name='g')
        h = tf.multiply(f, g, name='h')

    return h


def hardtanh(x, min_val, max_val):
    # activation for std
    with tf.variable_scope('hardtanh'):
        min_v = tf.fill(tf.shape(x), min_val)
        max_v = tf.fill(tf.shape(x), max_val)
        out = tf.where(x < min_v, min_v, x)
        out = tf.where(out > max_v, max_v, x)
    return out
