"""MG 12/10/2019 Render reconstructed and generated images"""
import numpy as np
import os
import argparse
import seaborn as sns
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='render test data')
parser.add_argument('path', type=str,
                    help='Path to reconstructions and generations')
parser.add_argument('--type', default='mnist',
                    choices=['mnist', 'synthetic'],
                    help='mnist: mnist binary data,\
                          synthetic: synthetic mixture')
args = parser.parse_args()

def render_mnist(path, img_size=784, width=28, num_plot=50):
    orig_data = np.fromfile(os.path.join(path, 'orig'),
                            dtype='float32', sep=' ')
    rec_data = np.fromfile(os.path.join(path, 'reconstruct'),
                           dtype='float32', sep=' ')
    gen_data = np.fromfile(os.path.join(path, 'generate'),
                           dtype='float32', sep=' ')
    orig_data = orig_data.reshape([-1, img_size])
    orig_data = orig_data[:num_plot, :]
    rec_data = rec_data.reshape([-1, img_size])
    rec_data = rec_data[:num_plot, :]
    gen_data = gen_data.reshape([-1, img_size])
    gen_data = gen_data[:num_plot, :]

    plt_orig = orig_data[:10, :].reshape([-1, width])
    plt_rec = rec_data[:10, :].reshape([-1, width])
    plt_gen = gen_data[:10, :].reshape([-1, width])

    if num_plot > 10:
        for col in range(np.floor(num_plot / 10).astype(int) - 1):
            lower, upper = (col + 1) * 10, (col + 2) * 10
            if lower < orig_data.shape[0]:
                tmp_data = orig_data[lower:upper, :].reshape([-1, width])
                plt_orig = np.hstack((plt_orig, tmp_data))
                tmp_data = rec_data[lower:upper, :].reshape([-1, width])
                plt_rec = np.hstack((plt_rec, tmp_data))
            if lower < gen_data.shape[0]:
                tmp_data = gen_data[lower:upper, :].reshape([-1, width])
                plt_gen = np.hstack((plt_gen, tmp_data))

    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)

    ax1.set_axis_off()
    ax1.imshow(plt_orig, cmap='gray')
    ax1.set_title('Original data')

    ax2.set_axis_off()
    ax2.imshow(plt_rec, cmap='gray')
    ax2.set_title('Reconstructions')

    ax3.set_axis_off()
    ax3.imshow(plt_gen, cmap='gray')
    ax3.set_title('Generations')

    plt.show()


def render_synthetic(path):
    orig_data = np.fromfile(os.path.join(path, 'orig'),
                            dtype='float32', sep=' ')
    rec_data = np.fromfile(os.path.join(path, 'reconstruct'),
                           dtype='float32', sep=' ')
    gen_data = np.fromfile(os.path.join(path, 'generate'),
                           dtype='float32', sep=' ')

    sns.set()
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)

    sns.distplot(orig_data, ax=ax1)
    ax1.set_title('Original data')
    ax1.set(xlabel='x', xlim=((orig_data.min(), orig_data.max())))

    sns.distplot(rec_data, ax=ax2)
    ax2.set_title('Reconstructions')
    ax2.set(xlabel='x', xlim=((orig_data.min(), orig_data.max())))

    sns.distplot(gen_data, ax=ax3)
    ax3.set_title('Generations')
    ax3.set(xlabel='x', xlim=((orig_data.min(), orig_data.max())))

    plt.show()



if __name__ == '__main__':
    if args.type == 'mnist':
        render_mnist(args.path)
    elif args.type == 'synthetic':
        render_synthetic(args.path)
    else:
        raise '{} is not a valid type of data!'.format(args.type)
