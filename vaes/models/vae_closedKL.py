"""MG 12/10/2019 Tensorflow implementation of vae
with some special features from Tomcaks vamp prior
but closed form KL (not empirical from one sample of z)"""
from models.Model import Model
import tensorflow as tf


class VAE(Model):
    """VAE model with gated dense layers as Tomczaks vampPrior"""

    def __init__(self, datasets, args, gph):
        super(VAE, self).__init__(datasets, args, gph)
        self.config.warmup = 1
        print('############ Model vae_closedKL')

    def _graph_loss(self, data_batch, p_x_logits, q_z_params, z_q):
        q_z_mean, q_z_logvar = q_z_params

        with tf.name_scope('loss'):
            get_mean = tf.placeholder_with_default(True, shape=[],
                                                   name='get_mean')

            # reconstruction loss
            if self.config.data == 'mnist':
                RE = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits
                                   (labels=data_batch, logits=p_x_logits), 1)
            elif self.config.data == 'synthetic':
                RE = 0.5 * tf.reduce_sum(tf.squared_difference(data_batch,
                                                               p_x_logits), 1)
            else:
                raise 'No loss defined for data {}!'.format(self.config.data)
            # KL
            KL = 0.5 * tf.reduce_sum(tf.exp(q_z_logvar) +
                                     tf.square(q_z_mean) - 1.0 -
                                     q_z_logvar, 1),

            RE = tf.cond(get_mean,
                         lambda: tf.reduce_mean(RE),
                         lambda: RE, name='RE')
            KL = tf.cond(get_mean,
                         lambda: tf.reduce_mean(KL),
                         lambda: KL, name='KL')
            loss = tf.add(RE, self.beta * KL, name='loss')
            tf.summary.scalar('RE_plot', RE)
            tf.summary.scalar('KL_plot', KL)
            tf.summary.scalar('loss_plot', loss)

            losses = (loss, RE, KL)

        return losses
