"""MG 29/06/2019 Tensorflow implementation of simple vae"""
import tensorflow as tf
from tensorflow.keras import layers
import tensorflow_probability as tfp
import numpy as np
import scipy.stats
import shutil
tfd = tfp.distributions
stats = scipy.stats


class VAE(object):
    """Simple VAE model"""

    def __init__(self, datasets, args, gph):
        print('############ Model vae')
        self.config = args                # model config
        self.gph = gph                  # model graph
        self.sess = None                # model session
        self.graph_ops = None           # graph operations
        self.train_op = None            # training operation
        self.summary_merge = None       # tb summaries
        self.saver = None               # model saver

        self.global_step = tf.Variable(initial_value=0, name='gstep',
                                       trainable=False, dtype=tf.int32)
        self.global_epoch = tf.Variable(initial_value=0, name='gepoch',
                                        trainable=False, dtype=tf.int32)
        self.create_graph(datasets)

    def _graph_iterators(self, datasets):
        with tf.variable_scope('data'):
            dt = datasets['train']
            iterator = tf.data.Iterator.from_structure(dt.output_types,
                                                       dt.output_shapes)
            data_batch = iterator.get_next()

            initializors = {
                'train': iterator.make_initializer(datasets['train']),
                'valid': iterator.make_initializer(datasets['valid']),
                'test': iterator.make_initializer(datasets['test'])
            }

        return data_batch, initializors

    def _graph_encoder(self, data_batch, z_size=8, number_of_nodes=64):
        with tf.variable_scope('encoder'):
            h1 = layers.Dense(units=number_of_nodes,
                              activation=tf.nn.relu,
                              use_bias=True,
                              kernel_initializer=tf.glorot_normal_initializer,
                              name='h1')(data_batch)
            h2 = layers.Dense(units=number_of_nodes,
                              activation=tf.nn.relu,
                              use_bias=True,
                              kernel_initializer=tf.glorot_normal_initializer,
                              name='h2')(h1)
        z_mean = layers.Dense(units=z_size,
                              use_bias=True,
                              kernel_initializer=tf.glorot_normal_initializer,
                              name='z_mean')(h2)
        z_logstd = layers.Dense(units=z_size,
                                use_bias=True,
                                kernel_initializer=tf.glorot_normal_initializer,
                                name='z_logstd')(h2)
        z_params = (z_mean, z_logstd)

        return z_params

    def _graph_latent(self, z_params):
        z_mean, z_logstd = z_params

        with tf.variable_scope('z'):
            eps = tf.random_normal(shape=tf.shape(z_mean), name='eps')
            z = tf.add(z_mean, tf.exp(z_logstd) * eps, name='z')

        return z

    def _graph_decoder(self, z, img_size=784, number_of_nodes=64):
        with tf.variable_scope('decoder'):
            h1 = layers.Dense(units=number_of_nodes,
                              activation=tf.nn.relu,
                              use_bias=True,
                              kernel_initializer=tf.glorot_normal_initializer,
                              name='h1')(z)
            h2 = layers.Dense(units=number_of_nodes,
                              activation=tf.nn.relu,
                              use_bias=True,
                              kernel_initializer=tf.glorot_normal_initializer,
                              name='h2')(h1)
        pred_logit = layers.Dense(units=img_size,
                                  use_bias=True,
                                  kernel_initializer=tf.glorot_normal_initializer,
                                  name='pred_logit')(h2)

        return pred_logit

    def _graph_loss(self, data_batch, pred_logit, z_params):
        z_mean, z_logstd = z_params

        with tf.variable_scope('loss'):
            get_mean = tf.placeholder_with_default(True, shape=[],
                                                   name='get_mean')

            # reconstruction loss
            loss_rec = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits
                                     (labels=data_batch, logits=pred_logit), 1)
            loss_kl = 0.5 * tf.reduce_sum(tf.exp(2.0 * z_logstd) +
                                          tf.square(z_mean) - 1.0 -
                                          2.0 * z_logstd, 1),

            loss_rec = tf.cond(get_mean,
                               lambda: tf.reduce_mean(loss_rec),
                               lambda: loss_rec, name='rec')
            loss_kl = tf.cond(get_mean,
                              lambda: tf.reduce_mean(loss_kl),
                              lambda: loss_kl, name='kl')
            loss_total = tf.add(loss_rec, loss_kl, name='total')
            tf.summary.scalar('loss_reconstruction', loss_rec)
            tf.summary.scalar('loss_kl', loss_kl)
            tf.summary.scalar('loss_total', loss_total)

            losses = (loss_total, loss_rec, loss_kl)

            return losses

    def _graph_likelihood(self, z_params, z):
        z_mean, z_logstd = z_params

        # distributions for log likelihood evaluation
        with tf.variable_scope('likelihood'):
            with tf.variable_scope('prior'):
                loc, scale = tf.zeros_like(z_mean), tf.ones_like(z_logstd)
                prior = tfd.MultivariateNormalDiag(loc=loc, scale_diag=scale,
                                                   name='distrib')
                piror_logprob = prior.log_prob(z, name='logprob')
            with tf.variable_scope('posterior'):
                loc, scale = z_mean, tf.exp(z_logstd)
                posterior = tfd.MultivariateNormalDiag(loc=loc,
                                                       scale_diag=scale,
                                                       name='distrib')
                post_logprob = posterior.log_prob(z, name='logprob')

            logratio = tf.subtract(piror_logprob, post_logprob,
                                   name='logratio')

        return logratio

    def create_graph(self, datasets):
        print('############ Creating graph')
        # data handling
        data_batch, initializors = self._graph_iterators(datasets)

        # vae
        z_params = self._graph_encoder(data_batch, self.config.z_size,
                                       self.config.number_of_nodes)
        z = self._graph_latent(z_params)
        pred_logit = self._graph_decoder(z, self.config.img_size,
                                         self.config.number_of_nodes)
        losses = self._graph_loss(data_batch, pred_logit, z_params)

        # test log likelihood
        logratio = self._graph_likelihood(z_params, z)

        # optimizer
        optimizer = tf.train.AdamOptimizer(self.config.learning_rate)
        self.train_op = optimizer.minimize(losses[0],
                                           global_step=self.global_step)

        # tb sumaries
        self.summary_merge = tf.summary.merge_all()

        self.graph_ops = (data_batch, initializors, z_params, z, pred_logit,
                          losses, logratio)

    def restore_model(self):
        tb_path, saver_path, print_file = self.config.pathes
        try:
            ckpt = tf.train.latest_checkpoint(saver_path)
            self.saver.restore(self.sess, save_path=ckpt)
            print('############ Restored variables from {}'.format(saver_path))
        except ValueError:
            print('############ Error: Nothing to restore from {}'
                  .format(saver_path))
            raise

        if self.config.train:   # remove old models if will train
            shutil.rmtree(saver_path)

    def train(self):
        print('############ Training')
        tb_path, saver_path, print_file = self.config.pathes
        (data_batch, initializors, z_params, z, pred_logit, losses,
         logratio) = self.graph_ops
        loss_total, loss_rec, loss_kl = losses
        f = open(print_file, "a")
        f.write('######################################################### \n')
        f.write('############ Training \n')

        next_epoch = tf.assign(self.global_epoch,
                               self.global_epoch + 1)

        if not self.config.restore:
            print('############ Initializing variables')
            f.write('############ Initializing variables \n')
            self.sess.run(tf.global_variables_initializer())
        else:
            print('############ Using restored variables')
            f.write('############ Using restored variables\n')

        _gepoch = self.sess.run(self.global_epoch)
        print('############ Starting from epoch {}'.format(_gepoch + 1))
        f.write('############ Starting from epoch {} \n'.format(_gepoch + 1))

        # tb writers
        writer_train = tf.summary.FileWriter(tb_path + '/train', self.gph)
        writer_valid = tf.summary.FileWriter(tb_path + '/valid')

        for _ in range(self.config.num_epochs):
            _, _gepoch = self.sess.run([initializors['train'], next_epoch])
            while True:
                try:
                    _loss_total, _, g_step = self.sess.run([loss_total,
                                                            self.train_op,
                                                            self.global_step])

                    # tb writers train
                    if g_step % self.config.write_every == 0:
                        _summary = self.sess.run(self.summary_merge)
                        writer_train.add_summary(_summary, g_step)

                except tf.errors.OutOfRangeError:   # beyond last batch
                    print('Epoch {}, loss {}'.format(_gepoch, _loss_total))

                    # tb writers valid
                    self.sess.run(initializors['valid'])
                    _summary = self.sess.run(self.summary_merge)
                    writer_valid.add_summary(_summary, g_step)

                    # saver
                    on_every = _gepoch % self.config.save_every == 0
                    last_epoch = _gepoch == self.config.num_epochs - 1
                    if on_every or last_epoch:
                        model_path = saver_path + '/model'
                        self.saver.save(self.sess, model_path,
                                        global_step=_gepoch)
                        print('Saved model to {}'.format(model_path))

                    break

        f.close()
        writer_train.close()
        writer_valid.close()

    def test(self):
        print('############ Testing')
        tb_path, saver_path, print_file = self.config.pathes
        (data_batch, initializors, z_params, z, pred_logit, losses,
         logratio) = self.graph_ops
        loss_total, loss_rec, loss_kl = losses
        f = open(print_file, "a")
        f.write('######################################################### \n')
        f.write('############ Testing \n')

        # test elbo
        self.sess.run(initializors['test'])
        _data_batch, _pred_logit, _loss_total, _loss_rec, _z_params = \
            self.sess.run([data_batch, pred_logit, loss_total, loss_rec,
                           z_params])

        # test log likelihood via importance sampling
        log_likelihood = self._get_likelihood(loss_rec, logratio, _z_params,
                                              _data_batch)

        # tb writer summary text
        tb_text = tf.constant('Elbo: ')
        tb_text = tf.string_join([tb_text, tf.as_string(loss_total)])
        tb_text = tf.string_join([tb_text,
                                  tf.constant('Reconstruction error: ')],
                                 separator="   |   ")
        tb_text = tf.string_join([tb_text,
                                  tf.as_string(loss_rec)])
        tb_text = tf.string_join([tb_text,
                                  tf.constant('Log likelihood: ')],
                                 separator="   |   ")
        tb_text = tf.string_join([tb_text,
                                  tf.as_string(tf.constant(log_likelihood))])
        sum_text = tf.summary.text('test_results', tb_text)

        # tb writer test reconstructions
        num_imgs = self.config.num_images
        writer_test = tf.summary.FileWriter(tb_path + '/test')
        img_dim = np.sqrt(self.config.img_size).astype(int)
        img_orig = _data_batch[:num_imgs]
        img_orig = img_orig.reshape(img_dim * num_imgs, img_dim)
        img_pred = scipy.special.expit(_pred_logit[:num_imgs])
        img_pred = img_pred.reshape(img_dim * num_imgs, img_dim)
        img = np.hstack((img_orig, img_pred))
        sum_rec = tf.summary.image('reconstruct',
                                   img[np.newaxis, :, :, np.newaxis])

        _sum_rec, _sum_text, _gepoch = self.sess.run([sum_rec, sum_text,
                                                      self.global_epoch])

        print('For testing using model from epoch {}'.format(_gepoch))
        f.write('For testing using model from epoch {} \n'.format(_gepoch))
        print('Test elbo {}'.format(_loss_total))
        f.write('Test elbo {} \n'.format(_loss_total))
        print('Test reconstruction {}'.format(_loss_rec))
        f.write('Test reconstruction {} \n'.format(_loss_rec))
        print('Test loglikelihood {}'.format(log_likelihood))
        f.write('Test loglikelihood {} \n'.format(log_likelihood))

        f.close()
        writer_test.add_summary(_sum_rec, _gepoch)
        writer_test.add_summary(_sum_text, _gepoch)
        writer_test.close()

    def generate(self):
        print('############ Generating')
        tb_path, saver_path, _ = self.config.pathes
        (data_batch, initializors, z_params, z, pred_logit, losses,
         logratio) = self.graph_ops
        num_imgs, z_size = self.config.num_images, self.config.z_size

        z_from_prior = np.random.normal(size=(num_imgs, z_size))
        _pred_logit = self.sess.run(pred_logit,
                                    feed_dict={'z/z:0': z_from_prior})

        # tb writer test generations
        writer_gen = tf.summary.FileWriter(tb_path + '/test')
        img_dim = np.sqrt(self.config.img_size).astype(int)
        img_pred = scipy.special.expit(_pred_logit[:num_imgs])
        img_pred = img_pred.reshape(img_dim * num_imgs, img_dim)
        img_pred = img_pred[np.newaxis, :, :, np.newaxis]
        sum_gen = tf.summary.image('generate', img_pred)
        _sum_gen, _gepoch = self.sess.run([sum_gen, self.global_epoch])
        writer_gen.add_summary(_sum_gen, _gepoch)
        writer_gen.close()

    def _get_likelihood(self, loss_rec, logratio, _z_params, _data_batch):
        _z_mean, _z_logstd = _z_params
        _batch_size = _z_mean.shape[0]

        num_resamples = self.config.likelihood_resamples
        logs = np.zeros([_batch_size, num_resamples])
        for resample in range(num_resamples):
            _loss_rec, _logratio = \
                self.sess.run([loss_rec, logratio],
                              feed_dict={'data/IteratorGetNext:0': _data_batch,
                                         'loss/get_mean:0': False,
                                         'z_mean/BiasAdd:0': _z_mean,
                                         'z_logstd/BiasAdd:0': _z_logstd})
            logs[:, resample] = -_loss_rec + _logratio

        log_like_instance = scipy.misc.logsumexp(logs, axis=1)
        log_like_instance -= np.log(num_resamples)
        log_likelihood = -np.mean(log_like_instance)

        return log_likelihood



