"""MG 25/07/2019 Tensorflow implementation of vae
with some special features from Tomcaks vamp prior"""
import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np
import os
import scipy.stats
import shutil
from utils import layer_gated_dense, hardtanh
tfd = tfp.distributions
stats = scipy.stats
layers = tf.layers


class Model(object):
    """Superclass for VAE models"""

    def __init__(self, datasets, args, gph):
        self.config = args              # model config
        self.gph = gph                  # model graph
        self.sess = None                # model session
        self.graph_ops = None           # graph operations
        self.train_op = None            # training operation
        self.summary_merge = None       # tb summaries
        self.saver = None               # model saver
        self.pseudo_ins = None          # vampprior pseudoinputs
        self.prior_distrib = None       # prior distribution
        self.logfile = None             # file for log print

        self.global_step = tf.Variable(initial_value=0, name='gstep',
                                       trainable=False, dtype=tf.int32)
        self.global_epoch = tf.Variable(initial_value=0, name='gepoch',
                                        trainable=False, dtype=tf.int32)
        # weighting for the kl
        self.beta = tf.Variable(initial_value=0., name='beta',
                                trainable=False, dtype=tf.float32)

        self.create_graph(datasets)

    def _graph_iterators(self, datasets):
        with tf.variable_scope('data'):
            dt = datasets['train']
            iterator = tf.data.Iterator.from_structure(dt.output_types,
                                                       dt.output_shapes)
            data_batch = iterator.get_next()

            initializors = {
                'train': iterator.make_initializer(datasets['train']),
                'valid': iterator.make_initializer(datasets['valid']),
                'test': iterator.make_initializer(datasets['test'])
            }

        return data_batch, initializors

    def _graph_encoder(self, data_batch, z_size, number_of_nodes, share=None):
        glorot = tf.glorot_normal_initializer
        with tf.variable_scope('q_z_x', reuse=share):
            # gated dense layers
            h1 = layer_gated_dense(data_batch, number_of_nodes, 'h1')
            h2 = layer_gated_dense(h1, number_of_nodes, 'h2')
            q_z_mean = layers.dense(h2, units=z_size,
                                    use_bias=True,
                                    kernel_initializer=glorot,
                                    name='q_z_mean')
            with tf.variable_scope('q_z_logvar'):
                q_z_logvar = layers.dense(h2, units=z_size,
                                          kernel_initializer=glorot,
                                          use_bias=True)
                q_z_logvar = hardtanh(q_z_logvar, -6., 2.)
                q_z_logvar = tf.identity(q_z_logvar, name='q_z_logvar')

        q_z_params = (q_z_mean, q_z_logvar)

        return q_z_params

    # def _graph_latent(self, q_z_params):
    #     q_z_mean, q_z_logvar = q_z_params

    #     with tf.variable_scope('z_q'):
    #         eps = tf.random_normal(shape=tf.shape(q_z_mean), name='eps')
    #         z_q = tf.add(q_z_mean, tf.exp(0.5 * q_z_logvar) * eps, name='z_q')

    #     return z_q

    def _graph_latent(self, q_z_params):
        q_z_mean, q_z_logvar = q_z_params

        with tf.variable_scope('z_q'):
            loc = tf.zeros(self.config.z_size)
            scale = tf.ones(self.config.z_size)
            eps_distrib = tfd.MultivariateNormalDiag(loc, scale)
            eps = eps_distrib.sample(tf.shape(q_z_mean)[0])
            z_q = tf.add(q_z_mean, tf.exp(0.5 * q_z_logvar) * eps, name='z_q')

        return z_q

    def _graph_decoder(self, z, img_size, number_of_nodes):
        with tf.variable_scope('p_x_z'):
            # gated dense layers
            h1 = layer_gated_dense(z, number_of_nodes, 'h1')
            h2 = layer_gated_dense(h1, number_of_nodes, 'h2')
            _k_init = tf.glorot_normal_initializer
            p_x_logits = layers.dense(h2, units=img_size,
                                      use_bias=True,
                                      kernel_initializer=_k_init,
                                      name='p_x_logits')

        return p_x_logits

    def _graph_pseudoinputs(self):
        pass

    # prior
    def _log_prior(self, z):
        with tf.variable_scope('prior'):
            loc = tf.zeros(self.config.z_size)
            scale = tf.ones(self.config.z_size)
            p_z = tfd.MultivariateNormalDiag(loc, scale)
            log_p_z = p_z.log_prob(z, name='log_p_z')

            self.prior_distrib = p_z
            return log_p_z

    # posterior
    def _log_posterior(self, q_z_params, z):
        q_z_mean, q_z_logvar = q_z_params
        with tf.variable_scope('posterior'):
            loc = q_z_mean
            scale = tf.exp(0.5 * q_z_logvar)
            q_z_x = tfd.MultivariateNormalDiag(loc, scale)
            log_q_z_x = q_z_x.log_prob(z, name='log_q_z_x')

            return log_q_z_x

    def _graph_loss(self, data_batch, p_x_logits, q_z_params, z_q):
        # q_z_mean, q_z_logvar = q_z_params

        with tf.name_scope('loss'):
            get_mean = tf.placeholder_with_default(True, shape=[],
                                                   name='get_mean')

            # reconstruction loss
            if self.config.data == 'mnist':
                RE = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits
                                   (labels=data_batch, logits=p_x_logits), 1)
            elif self.config.data == 'synthetic':
                RE = 0.5 * tf.reduce_sum(tf.squared_difference(data_batch,
                                                               p_x_logits), 1)
            else:
                raise 'No loss defined for data {}!'.format(self.config.data)
            # KL
            # log_p_z = self._log_prior(z_q)
            # log_q_z_x = self._log_posterior(q_z_params, z_q)
            # KL = log_q_z_x - log_p_z
            KL = self._logratio(q_z_params, z_q)

            # KL = 0.5 * tf.reduce_sum(tf.exp(q_z_logvar) +
            #                          tf.square(q_z_mean) - 1.0 -
            #                          q_z_logvar, 1),

            RE = tf.cond(get_mean,
                         lambda: tf.reduce_mean(RE),
                         lambda: RE, name='RE')
            KL = tf.cond(get_mean,
                         lambda: tf.reduce_mean(KL),
                         lambda: KL, name='KL')
            loss = tf.add(RE, self.beta * KL, name='loss')
            tf.summary.scalar('RE_plot', RE)
            tf.summary.scalar('KL_plot', KL)
            tf.summary.scalar('loss_plot', loss)

            losses = (loss, RE, KL)

        return losses

    def _logratio(self, q_z_params, z):

        # distributions for log likelihood evaluation
        with tf.name_scope('logratio'):
            prior_logprob = self._log_prior(z)
            post_logprob = self._log_posterior(q_z_params, z)
            logratio = tf.subtract(post_logprob, prior_logprob,
                                   name='ratio')

        return logratio

    def _epoch_counters(self):
        # global epoch counter
        with tf.variable_scope('gepoch_update'):
            g_update = self.global_epoch + 1
            next_epoch = tf.assign(self.global_epoch, g_update)
        # regularization weight for beta VAE
        with tf.variable_scope('beta_update'):
            gepoch = tf.cast(self.global_epoch, dtype=tf.float32)
            b_update = tf.minimum(1. * gepoch / self.config.warmup, 1.)
            beta_update = tf.assign(self.beta, b_update)

        return next_epoch, beta_update

    def create_graph(self, datasets):
        print('############ Creating graph')
        # data handling
        data_batch, initializors = self._graph_iterators(datasets)

        # vae
        q_z_params = self._graph_encoder(data_batch, self.config.z_size,
                                         self.config.number_of_nodes)
        z_q = self._graph_latent(q_z_params)
        p_x_logits = self._graph_decoder(z_q, self.config.img_size,
                                         self.config.number_of_nodes)
        self._graph_pseudoinputs()
        losses = self._graph_loss(data_batch, p_x_logits, q_z_params, z_q)

        # test log likelihood
        logratio = self._logratio(q_z_params, z_q)

        # optimizer
        optimizer = tf.train.AdamOptimizer(self.config.learning_rate)
        self.train_op = optimizer.minimize(losses[0],
                                           global_step=self.global_step)

        # epoch counters
        next_epoch, beta_update = self._epoch_counters()
        tf.summary.scalar('beta', self.beta)

        # tb sumaries
        self.summary_merge = tf.summary.merge_all()

        # cash graph operations
        self.graph_ops = {'data_batch': data_batch,
                          'initializors': initializors,
                          'q_z_params': q_z_params,
                          'z_q': z_q,
                          'p_x_logits': p_x_logits,
                          'losses': losses,
                          'logratio': logratio,
                          'next_epoch': next_epoch,
                          'beta_update': beta_update}

    def restore_model(self):
        tb_path, saver_path, print_file = self.config.pathes

        try:
            ckpt = tf.train.latest_checkpoint(saver_path)
            self.saver.restore(self.sess, save_path=ckpt)
            print('############ Restored variables from {}'.format(saver_path))
        except ValueError:
            print('############ Error: Nothing to restore from {}'
                  .format(saver_path))
            raise

        if self.config.train:   # if training, remove old models
            shutil.rmtree(saver_path)

    def train(self):
        tb_path, saver_path, print_path = self.config.pathes
        initializors = self.graph_ops['initializors']
        losses = self.graph_ops['losses']
        next_epoch = self.graph_ops['next_epoch']
        beta_update = self.graph_ops['beta_update']
        loss, RE, KL = losses
        self.logfile = os.path.join(print_path, 'log')

        f = open(self.logfile, "a")
        f.write('{}\n'.format(self.logfile))
        f.write('######################################################### \n')
        for arg in vars(self.config):
            print('{} {}'.format(arg, getattr(self.config, arg)))
            f.write('{} {} \n'.format(arg, getattr(self.config, arg)))
        f.write('######################################################### \n')
        print('############ Training')
        f.write('############ Training \n')

        if not self.config.restore:
            print('############ Initializing variables')
            f.write('############ Initializing variables \n')
            self.sess.run(tf.global_variables_initializer())
        else:
            print('############ Using restored variables')
            f.write('############ Using restored variables\n')

        _gepoch = self.sess.run(self.global_epoch)
        print('############ Starting from epoch {}'.format(_gepoch + 1))
        f.write('############ Starting from epoch {} \n'.format(_gepoch + 1))

        # tb writers
        writer_train = tf.summary.FileWriter(tb_path + '/train', self.gph)
        writer_valid = tf.summary.FileWriter(tb_path + '/valid')

        # main training loop
        for _ in range(self.config.num_epochs):
            _, _gepoch, _beta = self.sess.run([initializors['train'],
                                              next_epoch,
                                              beta_update])

            while True:
                try:
                    _losses, _, g_step, = self.sess.run([losses,
                                                         self.train_op,
                                                         self.global_step])

                    # tb writers train
                    if g_step % self.config.write_every == 0:
                        _summary = self.sess.run(self.summary_merge)
                        writer_train.add_summary(_summary, g_step)

                except tf.errors.OutOfRangeError:   # beyond last batch
                    print('Epoch {}, loss {}, re {}, kl {}, beta {}'
                          .format(_gepoch, _losses[0], _losses[1],
                                  _losses[2], _beta))
                    f.write('Epoch {}, loss {}, re {}, kl {}, beta {} \n'
                            .format(_gepoch, _losses[0], _losses[1],
                                    _losses[2], _beta))

                    # tb writers valid
                    self.sess.run(initializors['valid'])
                    _summary = self.sess.run(self.summary_merge)
                    writer_valid.add_summary(_summary, g_step)

                    # saver
                    on_every = _gepoch % self.config.save_every == 0
                    last_epoch = _gepoch == self.config.num_epochs
                    if on_every or last_epoch:
                        model_path = saver_path + '/model'
                        self.saver.save(self.sess, model_path,
                                        global_step=_gepoch)
                        print('Saved model to {}'.format(model_path))

                    break

        print('############ Finished after epoch {}'.format(_gepoch))
        f.write('############ Finished after epoch {} \n'.format(_gepoch))
        f.close()
        writer_train.close()
        writer_valid.close()

    def test(self):
        _, _, print_path = self.config.pathes
        data_batch = self.graph_ops['data_batch']
        initializors = self.graph_ops['initializors']
        q_z_params = self.graph_ops['q_z_params']
        p_x_logits = self.graph_ops['p_x_logits']
        losses = self.graph_ops['losses']
        logratio = self.graph_ops['logratio']
        loss, RE, KL = losses
        if self.logfile is None:
            self.logfile = os.path.join(print_path, 'log')

        print('############ Testing')
        f = open(self.logfile, "a")
        f.write('######################################################### \n')
        f.write('############ Testing \n')

        # test elbo
        self.sess.run(initializors['test'])
        (_data_batch,
         _p_x_logits,
         _loss,
         _RE,
         _KL,
         _q_z_params,
         _gepoch) = self.sess.run([data_batch,
                                   p_x_logits,
                                   loss,
                                   RE,
                                   KL,
                                   q_z_params,
                                   self.global_epoch])

        # test log likelihood via importance sampling
        log_likelihood = self._get_likelihood(RE, logratio, _q_z_params,
                                              _data_batch)

        # tb writer test reconstructions
        print('############ Reconstructing')
        f.write('############ Reconstructing \n')

        # save original images to file
        orig_file = os.path.join(print_path, 'orig')
        _data_batch.tofile(orig_file, sep=" ")
        # save reconstructed images to file
        rec_file = os.path.join(print_path, 'reconstruct')
        if self.config.data == 'mnist':
            img_pred = scipy.special.expit(_p_x_logits)
        elif self.config.data == 'synthetic':
            img_pred = _p_x_logits
        else:
            raise 'No save protocol for data {}!'.format(self.config.data)
        img_pred.tofile(rec_file, sep=" ")

        # print out summary results
        f.write('######################################################### \n')
        print('For testing using model from epoch {}'.format(_gepoch))
        f.write('For testing using model from epoch {} \n'.format(_gepoch))
        print('Test elbo {}'.format(_loss))
        f.write('Test elbo {} \n'.format(_loss))
        print('Test RE {}'.format(_RE))
        f.write('Test RE {} \n'.format(_RE))
        print('Test KL {}'.format(_KL))
        f.write('Test KL {} \n'.format(_KL))
        print('Test loglikelihood {}'.format(log_likelihood))
        f.write('Test loglikelihood {} \n'.format(log_likelihood))

        f.close()

    def generate(self):
        print('############ Generating')
        _, _, print_path = self.config.pathes
        p_x_logits = self.graph_ops['p_x_logits']
        num_imgs = self.config.num_images

        z_from_prior = self.prior_distrib.sample(num_imgs)
        _z_from_prior = self.sess.run(z_from_prior)
        _p_x_logits = self.sess.run(p_x_logits,
                                    feed_dict={'z_q/z_q:0': _z_from_prior})

        # save generated images to file
        gen_file = os.path.join(print_path, 'generate')
        if self.config.data == 'mnist':
            img_pred = scipy.special.expit(_p_x_logits)
        elif self.config.data == 'synthetic':
            img_pred = _p_x_logits
        else:
            raise 'No save protocol for data {}!'.format(self.config.data)
        img_pred.tofile(gen_file, sep=" ")

    def _get_likelihood(self, RE, logratio, _q_z_params, _data_batch):
        _q_z_mean, _q_z_logvar = _q_z_params
        _batch_size = _q_z_mean.shape[0]

        num_resamples = self.config.likelihood_resamples
        logs = np.zeros([_batch_size, num_resamples])
        _feed_dict = {'data/IteratorGetNext:0': _data_batch,
                      'loss/get_mean:0': False,
                      'q_z_x/q_z_mean/BiasAdd:0': _q_z_mean,
                      'q_z_x/q_z_logvar/q_z_logvar:0': _q_z_logvar}
        for resample in range(num_resamples):
            _RE, _logratio = self.sess.run([RE, logratio],
                                           feed_dict=_feed_dict)
            logs[:, resample] = -_RE - _logratio

        log_like_instance = scipy.misc.logsumexp(logs, axis=1)
        log_like_instance -= np.log(num_resamples)
        log_likelihood = -np.mean(log_like_instance)

        return log_likelihood
