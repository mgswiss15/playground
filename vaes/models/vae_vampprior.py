"""MG 25/07/2019 Tensorflow implementation of vae
with Tomcaks vamp prior"""
from models.Model import Model
import tensorflow as tf
import os
import tensorflow_probability as tfp
from utils import hardtanh
import numpy as np
import scipy.stats
tfd = tfp.distributions
layers = tf.layers


class VAE(Model):
    """VAE vampPrior of Tomczaks"""

    def __init__(self, datasets, args, gph):
        super(VAE, self).__init__(datasets, args, gph)
        print('############ Model vampPrior')

    def _graph_pseudoinputs(self):
        # vampPrior
        num_pseudo = self.config.num_pseudo
        with tf.variable_scope('p_z_u'):
            idle_inputs = tf.eye(num_pseudo, name='idle_inputs')
            _k_init = tf.truncated_normal_initializer(self.config.pseudo_mean,
                                                      self.config.pseudo_std)
            pseudo_ins = layers.dense(idle_inputs, units=self.config.img_size,
                                      use_bias=False,
                                      kernel_initializer=_k_init)
            pseudo_ins = hardtanh(pseudo_ins, 0.0, 1.0)

        self.pseudo_ins = pseudo_ins

    # prior
    def _log_prior(self, z):
        num_pseudo = self.config.num_pseudo
        q_z_params = self._graph_encoder(self.pseudo_ins,
                                         self.config.z_size,
                                         self.config.number_of_nodes,
                                         share=True)
        q_z_mean, q_z_logvar = q_z_params

        with tf.variable_scope('vampPrior'):
            loc = q_z_mean
            scale = tf.exp(0.5 * q_z_logvar)
            q_z_u = tfd.MultivariateNormalDiag(loc, scale)
            z = tf.expand_dims(z, axis=1)
            # z = tf.ones([100, 1, 40])
            log_q_z_u = q_z_u.log_prob(z, name='log_q_z_u')

            # acoundts for log 1/num_pseudo in the mixture
            log_q_z_u = log_q_z_u - tf.log(tf.cast(num_pseudo,
                                                   dtype=tf.float32))

            # log sum exp
            max_log_z = tf.reduce_max(log_q_z_u, axis=1, keepdims=True)

            log_p_z_u = tf.add(tf.squeeze(max_log_z),
                               tf.reduce_logsumexp(log_q_z_u - max_log_z,
                                                   axis=1), name='log_p_z_u')

        self.prior_distrib = q_z_u
        return log_p_z_u

    def generate(self):
        print('############ Generating')
        _, _, print_path = self.config.pathes
        p_x_logits = self.graph_ops['p_x_logits']
        num_imgs = self.config.num_images

        z_from_prior = self.prior_distrib.sample()
        _z_from_prior = self.sess.run(z_from_prior)
        rand_idx = np.random.choice(_z_from_prior.shape[0], size=num_imgs,
                                    replace=False)
        _z_from_prior = _z_from_prior[rand_idx, :]
        _p_x_logits = self.sess.run(p_x_logits,
                                    feed_dict={'z_q/z_q:0': _z_from_prior})

        # save generated images to file
        gen_file = os.path.join(print_path, 'generate')
        if self.config.data == 'mnist':
            img_pred = scipy.special.expit(_p_x_logits)
        elif self.config.data == 'synthetic':
            img_pred = _p_x_logits
        else:
            raise 'No save protocol for data {}!'.format(self.config.data)
        img_pred.tofile(gen_file, sep=" ")
