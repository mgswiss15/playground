"""MG 25/07/2019 Tensorflow implementation of vae
with some special features from Tomcaks vamp prior"""
from models.Model import Model
import os
import scipy.stats


class VAE(Model):
    """VAE model with gated dense layers as Tomczaks vampPrior"""

    def __init__(self, datasets, args, gph):
        super(VAE, self).__init__(datasets, args, gph)
        print('############ Model vae_tom')
