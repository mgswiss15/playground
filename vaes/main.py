"""MG 29/06/2019 Tensorflow implementation of vae vampprior"""

import tensorflow as tf
import argparse
from utils import load_mnist, load_synthetic, make_write_folders

parser = argparse.ArgumentParser(description='VAE priors')
# experiment
parser.add_argument('experiment_name', type=str,
                    help='Name of experiment')
parser.add_argument('--data', default='mnist',
                    choices=['mnist', 'synthetic'],
                    help='mnist: mnist binary data,\
                          synthetic: synthetic mixture')
# data
parser.add_argument('--img_size', type=int, default=784,
                    help='Image size in pixels (width * height)')
parser.add_argument('--batch_size', type=int, default=100,
                    help='Batch size in training')
# model
parser.add_argument('--model', default='vae_closedKL',
                    choices=['vae_tom', 'vampprior', 'vae_closedKL'],
                    help='vae_tom: vae as in Tomczaks vampprior,\
                          vampprior: vae with vampprior,\
                          vae_closedKL: vae as Tomczaks but closed form KL')
parser.add_argument('--number_of_nodes', type=int, default=300,
                    help='Size of MLP hidden layers')
parser.add_argument('--z_size', type=int, default=40,
                    help='Size of hidden variable')
parser.add_argument('--num_pseudo', type=int, default=500,
                    help='Number of pseudo-inputs for vampprior')
parser.add_argument('--pseudo_mean', type=float, default=-0.05,
                    help='Means of initial pseudo-inputs in vampprior')
parser.add_argument('--pseudo_std', type=float, default=0.01,
                    help='Std of initial pseudo-inputs for vampprior')
# run options
parser.add_argument('--learning_rate', type=float, default=0.0001,
                    help='Learning rate for ADAM optimizer')
parser.add_argument('--num_epochs', type=int, default=50,
                    help='Number of epochs')
parser.add_argument('--save_every', type=int, default=50,
                    help='Saver model every --save_every epochs')
# model updates
parser.add_argument('--restore_from', type=str, default=None,
                    help="Model to restore;  ='self' restore from run options")
parser.add_argument('--no_train', action='store_true', default=False,
                    help='Do not train model')
parser.add_argument('--no_test', action='store_true', default=False,
                    help='Do not test model')
parser.add_argument('--no_generate', action='store_true', default=False,
                    help='Do not generate from model')
# monitoring
parser.add_argument('--tb_folder', default='tb_graphs',
                    help='Folder to store tensorboard outputs')
parser.add_argument('--saver_folder', default='saved_models',
                    help='Folder to store model checkpoints')
parser.add_argument('--print_folder', default='print_results',
                    help='Folder to write model results')
parser.add_argument('--write_every', type=int, default=50,
                    help='Write train progress to tb every WRITE_EVERY steps')
parser.add_argument('--num_images', type=int, default=100,
                    help='Number of images to restore/generate')
parser.add_argument('--likelihood_resamples', type=int, default=50,
                    help='Number of resamples for likelihood estimation')
parser.add_argument('--run_id', default='',
                    help='Manual id for run; anything; end of folder names')
# beta warmup
parser.add_argument('--warmup', default='100.', type=float,
                    help='number of epochs to warmup beta VAE')


args = parser.parse_args()
args.train = not args.no_train
args.test = not args.no_test
args.generate = not args.no_generate
args.restore = args.restore_from is not None or (args.no_train and
                                                 (args.test or args.generate))

def run(args):
    # set-up monitoring
    args.pathes = make_write_folders(args)
    if args.restore and (args.restore_from is None or
                         args.restore_from == 'self'):
        _, args.restore_from, _ = args.pathes

    # initiate graph
    tf.reset_default_graph()
    gph = tf.get_default_graph()

    # get data
    if args.data == 'mnist':
        datasets = load_mnist(args.batch_size, args.img_size)
    elif args.data == 'synthetic':
        datasets = load_synthetic(args.batch_size)
        args.img_size = 1
    else:
        raise '{} cannot be loaded!'.format(args.data)

    # create model
    if args.model == 'vae_tom':
        from models.vae_tom import VAE
    elif args.model == 'vampprior':
        from models.vae_vampprior import VAE
    elif args.model == 'vae_closedKL':
        from models.vae_closedKL import VAE
    else:
        raise '{} is not a valid model name!'.format(args.model)

    model = VAE(datasets, args, gph)

    # debugging graph
    # print('==============================================')
    # print('List of all trainable variables')
    # variable_list = [v.name for v in tf.trainable_variables()]
    # for var in variable_list:
    #     print(var)
    # print('==============================================')

    # computation over graph
    model.sess = tf.Session(graph=model.gph)

    model.saver = tf.train.Saver()

    # restore model variables
    if args.restore_from is not None:
        model.restore_model()

    # train model
    if args.train:
        model.train()

    # test model
    if args.test:
        model.test()

    # generate images
    if args.generate:
        model.generate()

    model.sess.close()


if __name__ == '__main__':
    run(args)
