""" Multilayer perceptron network (feedforward network)

    Magda, 24/12/2018
"""

import tensorflow as tf
import numpy as np
import time
import os, shutil
import loaders


class FeedForwardNet(object):
    """Multilayer perceptron (feedforward net) for classification.

    Two hidden layers network with dropout in both hidden layers.
    """
    def __init__(self, restore_model=True, batch_size=64, data_load_func=None, num_hidden1=128,
                 num_hidden2=128, learn_rate=0.001, keep_prob=0.5, base_api=True):
        # training parameters
        self.restore_model = restore_model
        self.batch_size = batch_size
        self.learn_rate = learn_rate
        self.saver_folder = None
        # network structure
        self.base_api = base_api
        self.num_hidden1 = num_hidden1
        self.num_hidden2 = num_hidden2
        self.keep_prob = keep_prob
        self.train_mode = None
        # data
        self.data_load_func = data_load_func
        self.train_dataset = None
        self.val_dataset = None
        self.test_dataset = None
        self.img_size = None
        self.num_classes = None
        self.channels = None
        self.x_batch = None
        self.y_batch = None
        self.initializers = None
        # graph variables
        self.logits = None
        self.loss = None
        self.train_optim = None
        self.accuracy = None
        self.tb_summaries = None
        # build graph
        tf.reset_default_graph()
        self.gph = tf.get_default_graph()
        self.global_step = tf.Variable(initial_value=0, name='gstep', trainable=False, dtype=tf.int32)
        self.global_epochs = tf.Variable(initial_value=1, name='gepoch', trainable=False, dtype=tf.int32)
        self.keep_prob_ph = tf.placeholder(dtype=tf.float32, shape=[], name='keep_prob')
        self.build_model()

    def read_data(self):
        """Use load function to get data and prepare tensorflow dataset and iterators"""
        with tf.name_scope('data'):
            # load train/val/test data
            self.train_dataset, self.val_dataset, self.test_dataset, data_params = self.data_load_func(
                batch_size=self.batch_size)
            self.img_size = data_params[0]
            self.num_classes = data_params[1]
            self.channels = data_params[2]

            # create iterators
            iterator = tf.data.Iterator.from_structure(output_types=self.train_dataset.output_types,
                                                       output_shapes=self.train_dataset.output_shapes)
            self.x_batch, self.y_batch = iterator.get_next()

            self.initializers = {
                'train': iterator.make_initializer(self.train_dataset),  # initializer for train data
                'val': iterator.make_initializer(self.val_dataset),  # initializer for val data
                'test': iterator.make_initializer(self.test_dataset),  # initializer for test data
            }

    def build_graph_base(self):
        """Tow hidden-layers feed forward using base API"""
        # variables
        w1 = tf.get_variable('w1', shape=[self.img_size ** 2 * self.channels, self.num_hidden1],
                             initializer=tf.glorot_normal_initializer())
        b1 = tf.get_variable('b1', shape=[self.num_hidden1], initializer=tf.zeros_initializer())
        w2 = tf.get_variable('w2', shape=[self.num_hidden1, self.num_hidden2],
                             initializer=tf.glorot_normal_initializer())
        b2 = tf.get_variable('b2', shape=[self.num_hidden2], initializer=tf.zeros_initializer())
        w3 = tf.get_variable('w3', shape=[self.num_hidden2, self.num_classes],
                             initializer=tf.glorot_normal_initializer())
        b3 = tf.get_variable('b3', shape=[self.num_classes], initializer=tf.zeros_initializer())

        # layers
        x = tf.reshape(self.x_batch, [-1, self.img_size ** 2 * self.channels], name='flat_x')
        h1 = tf.nn.relu(tf.matmul(x, w1) + b1, name='h1')
        h1 = tf.nn.dropout(h1, keep_prob=self.keep_prob_ph, name='drop_h1')

        h2 = tf.nn.relu(tf.matmul(h1, w2) + b2, name='h2')
        h2 = tf.nn.dropout(h2, keep_prob=self.keep_prob_ph, name='drop_h2')

        self.logits = tf.add(tf.matmul(h2, w3), b3, name='out')

    def build_graph(self):
        """Tow hidden-layers feed forward using higher-level API"""
        x = tf.layers.flatten(self.x_batch, name='flat_x')
        h1 = tf.layers.dense(x, units=self.num_hidden1, use_bias=True, kernel_initializer=tf.glorot_normal_initializer,
                             activation=tf.nn.relu, name='h1')
        h1 = tf.layers.dropout(h1, rate=self.keep_prob, training=self.train_mode, name='drop_h1')
        h2 = tf.layers.dense(h1, units=self.num_hidden2, use_bias=True, kernel_initializer=tf.glorot_normal_initializer,
                             activation=tf.nn.relu, name='h2')
        h2 = tf.layers.dropout(h2, rate=self.keep_prob, training=self.train_mode, name='drop_h2')
        self.logits = tf.layers.dense(h2, units=self.num_classes, use_bias=True,
                                      kernel_initializer=tf.glorot_normal_initializer, name='out')

    def get_loss(self):
        """Cross entropy loss (for training)"""
        with tf.name_scope('loss'):
            cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=self.y_batch, logits=self.logits,
                                                                       name='cross_entropy')
            self.loss = tf.reduce_mean(cross_entropy, name='loss')

    def get_accuracy(self):
        """Accuracy for model evaluation"""
        with tf.name_scope('accuracy'):
            matches = tf.equal(tf.argmax(self.logits, axis=1), tf.argmax(self.y_batch, axis=1), name='matches')
            self.accuracy = tf.reduce_mean(tf.to_float(matches), name='accuracy')

    def get_summaries(self):
        """Sumaries for tensorboard plotting"""
        self.tb_summaries = {
            'loss': tf.summary.scalar('loss', self.loss),
            'acc': tf.summary.scalar('accuracy', self.accuracy)
        }

    def create_optimizer(self):
        optim = tf.train.AdamOptimizer(learning_rate=self.learn_rate, name='optim')
        self.train_optim = optim.minimize(self.loss, global_step=self.global_step)

    def create_saver(self):
        """Where model checkpoints are stored"""
        saver_folder = './checkpoints/ffn_base/lr_' if self.base_api else './checkpoints/ffn_higher/lr_'
        self.saver_folder = saver_folder + str(self.learn_rate) + '_kp_' + str(self.keep_prob)

    def build_model(self):
        """Build complete inference model"""
        self.read_data()
        if self.base_api:
            self.build_graph_base()
        else:
            self.build_graph()
        self.get_loss()
        self.get_accuracy()
        self.get_summaries()
        self.create_optimizer()
        self.create_saver()

    def train_epoch(self, sess, epoch_id, writer):
        """Run one training epoch"""
        # dropout operates differently at train and test
        self.train_mode = True

        start_time = time.time()
        total_loss = 0
        train_accuracy = 0
        num_batches = 0

        sess.run(self.initializers['train'])
        while True:
            try:
                _, step_loss, step_accuracy = sess.run((self.train_optim, self.loss, self.accuracy),
                                                       feed_dict={self.keep_prob_ph: self.keep_prob})
                num_batches += 1
                total_loss += step_loss
                train_accuracy += step_accuracy

                # tb plotting
                step, tb_loss, tb_acc = sess.run(
                    (self.global_step, self.tb_summaries['loss'], self.tb_summaries['acc']),
                    feed_dict={self.keep_prob_ph: self.keep_prob})
                writer.add_summary(tb_loss, global_step=step)
                writer.add_summary(tb_acc, global_step=step)
            except tf.errors.OutOfRangeError:
                break

        print('Epoch {:d} finished in {:.4f} over {:d} batches, avg loss: {:.4f}, avg train accruacy: {:.4f}'.format(
            epoch_id, time.time() - start_time, num_batches, total_loss / num_batches, train_accuracy / num_batches))

    def validate(self, sess, writer):
        """One evaluation over whole validation set"""
        # dropout operates differently at train and test
        self.train_mode = False

        sess.run(self.initializers['val'])
        step_accuracy, tb_acc, step = sess.run((self.accuracy, self.tb_summaries['acc'], self.global_step),
                                               feed_dict={self.keep_prob_ph: 1})
        print('Avg val accuracy: {:4f}'.format(step_accuracy))
        writer.add_summary(tb_acc, global_step=step)

    def train_model(self, num_epochs, save_every):
        var_init = tf.global_variables_initializer()
        saver = tf.train.Saver()
        increment_global_epochs = tf.assign(self.global_epochs, self.global_epochs + 1)

        writer_folder = './tb_graphs/ffn_base' if self.base_api else './tb_graphs/ffn_higher'
        writer_train_folder = writer_folder + '/train/lr_' + str(self.learn_rate) + '_kp_' + str(self.keep_prob)
        writer_val_folder = writer_folder + '/val/lr_' + str(self.learn_rate) + '_kp_' + str(self.keep_prob)
        # tensorboard prefers having just one file withing the graphing folder
        if os.path.exists(writer_train_folder):
            shutil.rmtree(writer_train_folder)
        if os.path.exists(writer_val_folder):
            shutil.rmtree(writer_val_folder)
        writer_train = tf.summary.FileWriter(writer_train_folder, self.gph)
        writer_val = tf.summary.FileWriter(writer_val_folder)

        # main session
        sess = tf.Session()
        sess.run(var_init)

        # restore model from previous checkpoints
        ckpt = tf.train.latest_checkpoint(self.saver_folder)
        if not self.restore_model and os.path.exists(self.saver_folder):
            if os.path.exists(self.saver_folder + '_old'):
                shutil.rmtree(self.saver_folder + '_old')
            shutil.move(self.saver_folder, self.saver_folder + '_old')
            os.makedirs(self.saver_folder)
            print('Moved old checkpoints to ', self.saver_folder + '_old')
        elif ckpt:
            saver.restore(sess, save_path=ckpt)
            print('Restored models from ', ckpt)
        else:
            print('Nothing to restore from, starting from scratch.')
            os.makedirs(self.saver_folder)

        # update number of epochs
        epoch_start = self.global_epochs.eval(sess)
        print('Starting from epoch ', epoch_start)

        for epoch_id in range(epoch_start, epoch_start + num_epochs):
            self.train_epoch(sess, epoch_id, writer_train)
            self.validate(sess, writer_val)

            # increment counter for next restore
            global_epoch = sess.run(increment_global_epochs)

            # save model
            if epoch_id % save_every == 0 or epoch_id == num_epochs - 1:
                check_path = saver.save(sess, save_path=self.saver_folder + '/model', global_step=epoch_id)
                print('Saved model params to {}'.format(check_path))

        writer_train.close()
        writer_val.close()
        sess.close()

    def test_model(self):
        """Test existing model over test data"""
        # dropout operates differently at train and test
        self.train_mode = False

        saver = tf.train.Saver()
        sess = tf.Session()

        # restore model
        ckpt = tf.train.latest_checkpoint(self.saver_folder)
        if ckpt:
            saver.restore(sess, save_path=ckpt)
            print('Restored models from ', ckpt)
        else:
            print('No model to restore and test over.')
            return None

        test_accuracy = 0
        num_batches = 0

        sess.run(self.initializers['test'])
        while True:
            try:
                step_accuracy = sess.run(self.accuracy, feed_dict={self.keep_prob_ph: 1})
                test_accuracy += step_accuracy
                num_batches += 1
            except tf.errors.OutOfRangeError:
                break

        print('Avg test accuracy: {:4f}'.format(test_accuracy/num_batches))

if __name__ == '__main__':
    BASE_API = False
    NUM_EPOCHS = 20
    RESTORE_MODEL = True
    SAVE_EVERY = 5
    DATA_FUNC = loaders.load_mnist

    # instantiate network object
    ffnet = FeedForwardNet(restore_model=RESTORE_MODEL, data_load_func=DATA_FUNC, base_api=BASE_API)

    # you can train or not a model
    ffnet.train_model(num_epochs=NUM_EPOCHS, save_every=SAVE_EVERY)

    # you can test or not the model (won't do anything if a previous model is not stored)
    ffnet.test_model()
