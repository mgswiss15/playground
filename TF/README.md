# TF basic models
Here are some standard NN models developed in Tensorflow.

The objective is not to have perfect code for each but to learn by doing, progressively moving from simpler to more complex.

This means I'm not revisiting earlier code with ideas for better/ neater/ more efficient implementation I may have in the later code.

It also means the code is not super user friendly with plenty of user-options.
Typically it implements static network structure and only the simple things, such as num of hidden units or channels in cnn can be changed via parameters.

I'll put some comments here primarily for me to remember what I did, why and what perhaps would be good to do next.

These are all one-off models so not generic (e.g. the network structure in terms of number of layers is fixed)
but they do use some important useful tensorflow tricks such as

* Datasets for batch management
* Saver for saving model checkpoints
* Writer for saving model progress for display in tensorboard

### [Multilayer perceptron](mlp.md)

### [Convolutional neural network](cnn.md)

### [Variational autoencoder](vae.md)

### [Variational autoencoder with classification](cvae.md)
