""" VAE wtih dense layers

    Magda, 26/12/2018
"""

import tensorflow as tf
import numpy as np
import time
import os
import shutil
import loaders


class VaeDense(object):
    """VAE with dense encoder and decoder

    Both are two hidden layers network with dropouts.
    """

    def __init__(self, restore_model=True, batch_size=128, data_load_func=None, latent_dim=8, enc_num_hidden1=64,
                 enc_num_hidden2=64, dec_num_hidden1=64, dec_num_hidden2=64, learn_rate=0.001, keep_prob=0.5, z_resampling=30):
        # training parameters
        self.restore_model = restore_model
        self.batch_size = batch_size
        self.learn_rate = learn_rate
        self.z_resampling = z_resampling     # number of resamples of z variables for the decoder step
        # network structure
        self.latent_dim = latent_dim
        self.enc_num_hidden1 = enc_num_hidden1
        self.enc_num_hidden2 = enc_num_hidden2
        self.dec_num_hidden1 = dec_num_hidden1
        self.dec_num_hidden2 = dec_num_hidden2
        self.keep_prob = keep_prob
        self.train_mode = None
        # data
        self.data_load_func = data_load_func
        self.train_dataset = None
        self.val_dataset = None
        self.test_dataset = None
        self.img_size = None
        self.channels = None
        self.x_batch = None
        self.initializers = None
        # graph variables
        self.z = None
        self.eps_mult = None
        self.x_out = None
        self.loss_vae = None
        self.train_optim = None
        self.tb_summaries = None
        # build graph
        tf.reset_default_graph()
        self.gph = tf.get_default_graph()
        self.global_step = tf.Variable(initial_value=0, name='gstep', trainable=False, dtype=tf.int32)
        self.global_epochs = tf.Variable(initial_value=1, name='gepoch', trainable=False, dtype=tf.int32)
        self.build_model()

    def read_data(self):
        """Use load function to get data and prepare tensorflow dataset and iterators"""
        with tf.name_scope('data'):
            # load train/val/test data
            self.train_dataset, self.val_dataset, self.test_dataset, data_params = self.data_load_func(
                batch_size=self.batch_size)
            self.img_size = data_params[0]
            self.channels = data_params[2]

            # create iterators
            iterator = tf.data.Iterator.from_structure(output_types=self.train_dataset.output_types,
                                                       output_shapes=self.train_dataset.output_shapes)
            self.x_batch, _ = iterator.get_next()

            self.initializers = {
                'train': iterator.make_initializer(self.train_dataset),  # initializer for train data
                'val': iterator.make_initializer(self.val_dataset),  # initializer for val data
                'test': iterator.make_initializer(self.test_dataset),  # initializer for test data
            }

    def build_encoder(self):
        """Tow hidden-layers feed forward using higher-level API"""
        x = tf.layers.flatten(self.x_batch, name='flat_x')
        h1 = tf.layers.dense(x, units=self.enc_num_hidden1, use_bias=True,
                             kernel_initializer=tf.glorot_normal_initializer,
                             activation=tf.nn.relu, name='enc_h1')
        h1 = tf.layers.dropout(h1, rate=self.keep_prob, training=self.train_mode, name='drop_enc_h1')
        h2 = tf.layers.dense(h1, units=self.enc_num_hidden2, use_bias=True,
                             kernel_initializer=tf.glorot_normal_initializer,
                             activation=tf.nn.relu, name='enc_h2')
        h2 = tf.layers.dropout(h2, rate=self.keep_prob, training=self.train_mode, name='drop__enc_h2')
        z_mean = tf.layers.dense(h2, units=self.latent_dim, use_bias=True,
                                 kernel_initializer=tf.glorot_normal_initializer, name='z_mean')
        log_z_std = tf.layers.dense(h2, units=self.latent_dim, use_bias=True,
                                    kernel_initializer=tf.glorot_normal_initializer, name='log_z_std')
        return z_mean, log_z_std

    def get_latent(self, z_mean, log_z_std):
        with tf.name_scope('z'):
            # sampling of z
            # importance sampling of k=30
            self.z_resample = tf.constant(self.z_resampling, name='z_resample')
            eps = tf.random_normal(shape=(tf.shape(self.x_batch)[0]*self.z_resample, self.latent_dim), name='eps')

            # at train generate z as random variables so eps_mult=1.0
            # at test z is just mean so eps_mult=0 (through feed_dict in session)
            self.eps_mult = tf.constant(1.0, name='eps_mult')
            eps = tf.multiply(self.eps_mult, eps, name='eps_adjust')

            self.z = tf.add(tf.tile(z_mean, multiples=(self.z_resample, 1)), tf.exp(tf.tile(log_z_std, multiples=(self.z_resample, 1))) * eps, name='z')

    def build_decoder(self):
        """Tow hidden-layers feed forward using higher-level API"""
        h1 = tf.layers.dense(self.z, units=self.dec_num_hidden1, use_bias=True,
                             kernel_initializer=tf.glorot_normal_initializer,
                             activation=tf.nn.relu, name='dec_h1')
        h1 = tf.layers.dropout(h1, rate=self.keep_prob, training=self.train_mode, name='drop_dec_h1')
        h2 = tf.layers.dense(h1, units=self.dec_num_hidden2, use_bias=True,
                             kernel_initializer=tf.glorot_normal_initializer,
                             activation=tf.nn.relu, name='dec_h2')
        h2 = tf.layers.dropout(h2, rate=self.keep_prob, training=self.train_mode, name='drop__dec_h2')
        x_out = tf.layers.dense(h2, units=self.img_size ** 2 * self.channels, use_bias=True,
                                kernel_initializer=tf.glorot_normal_initializer, activation=tf.nn.sigmoid, name='x_out')
        self.x_out = tf.reshape(x_out, shape=[-1, self.img_size, self.img_size, self.channels])

    def get_loss(self, z_mean, log_z_std):
        """Negative elbo (for training)"""
        with tf.name_scope('loss'):
            loss_reconstruct = 0.5*tf.reduce_mean(
                tf.reduce_sum(tf.square(tf.layers.flatten(tf.tile(self.x_batch, multiples=(self.z_resample,1,1,1)) - self.x_out)), 1),
                name='cross_ent')  # squared error
            kl_divergence = tf.reduce_mean(
                0.5 * tf.reduce_sum(tf.exp(2 * log_z_std) + tf.square(z_mean) - 1 - 2 * log_z_std, 1), name='kl')
            self.loss_vae = tf.add(loss_reconstruct, kl_divergence, name='loss')

        return loss_reconstruct, kl_divergence

    def get_summaries(self, loss_reconstruct, kl_divergence):
        """Sumaries for tensorboard plotting"""
        tf.summary.scalar('loss_rec', loss_reconstruct),
        tf.summary.scalar('kl', kl_divergence)
        tf.summary.scalar('loss_vae', self.loss_vae)
        self.tb_summaries = tf.summary.merge_all()

    def create_optimizer(self):
        optim = tf.train.AdamOptimizer(learning_rate=self.learn_rate, name='optim')
        self.train_optim = optim.minimize(self.loss_vae, global_step=self.global_step)

    def build_model(self):
        """Build complete inference model"""
        self.read_data()
        z_mean, log_z_std = self.build_encoder()
        self.get_latent(z_mean, log_z_std)
        self.build_decoder()
        loss_reconstruct, kl_divergence = self.get_loss(z_mean, log_z_std)
        self.get_summaries(loss_reconstruct, kl_divergence)
        self.create_optimizer()

    def train_epoch(self, sess, epoch_id, writer):
        """Run one training epoch"""
        # dropout operates differently at train and test
        self.train_mode = True
        # train_eps = {self.eps_mult: np.ones(shape=(1,))}

        start_time = time.time()
        total_loss = 0
        num_batches = 0

        sess.run(self.initializers['train'])
        while True:
            try:
                _, step_loss = sess.run((self.train_optim, self.loss_vae))
                num_batches += 1
                total_loss += step_loss

                # tb plotting
                step, tb_loss_vae = sess.run((self.global_step, self.tb_summaries))
                writer.add_summary(tb_loss_vae, global_step=step)
            except tf.errors.OutOfRangeError:
                break

        print('Epoch {:d} finished in {:.4f} over {:d} batches, avg loss: {:.4f}'.format(
            epoch_id, time.time() - start_time, num_batches, total_loss / num_batches))

    def get_saver_folder(self):
        """Where model checkpoints are stored based on param configs"""
        saver_folder = './checkpoints/vae_dense/lr_' + str(self.learn_rate) + '_kp_' + str(self.keep_prob)
        saver_folder += '_bs_' + str(self.batch_size)
        saver_folder += '_ld_' + str(self.latent_dim)
        saver_folder += '_eh1_' + str(self.enc_num_hidden1)
        saver_folder += '_eh2_' + str(self.enc_num_hidden2)
        saver_folder += '_dh1_' + str(self.dec_num_hidden1)
        saver_folder += '_dh2_' + str(self.dec_num_hidden2)
        saver_folder += '_zr_' + str(self.z_resampling)

        if not self.restore_model and os.path.exists(saver_folder):
            if os.path.exists(saver_folder + '_old'):
                shutil.rmtree(saver_folder + '_old')
            shutil.move(saver_folder, saver_folder + '_old')
            print('Moved old checkpoints to ', saver_folder + '_old')

        return saver_folder

    def get_writer_folder(self):
        """Where tensorboard files are stored based on param configs"""
        writer_folder = './tb_graphs/vae_dense/lr_' + str(self.learn_rate) + '_kp_' + str(self.keep_prob)
        writer_folder += '_bs_' + str(self.batch_size)
        writer_folder += '_ld_' + str(self.latent_dim)
        writer_folder += '_eh1_' + str(self.enc_num_hidden1)
        writer_folder += '_eh2_' + str(self.enc_num_hidden2)
        writer_folder += '_dh1_' + str(self.dec_num_hidden1)
        writer_folder += '_dh2_' + str(self.dec_num_hidden2)
        writer_folder += '_zr_' + str(self.z_resampling)

        # clean old folders
        if not self.restore_model and os.path.exists(writer_folder):
            if os.path.exists(writer_folder + '_old'):
                shutil.rmtree(writer_folder + '_old')
            shutil.move(writer_folder, writer_folder + '_old')

        return writer_folder

    def train_model(self, num_epochs, save_every):
        var_init = tf.global_variables_initializer()
        saver = tf.train.Saver()
        saver_folder = self.get_saver_folder()
        increment_global_epochs = tf.assign(self.global_epochs, self.global_epochs + 1)

        writer_folder = self.get_writer_folder()
        writer = tf.summary.FileWriter(writer_folder, self.gph)

        # main session
        sess = tf.Session()
        sess.run(var_init)

        # restore model from previous checkpoints
        ckpt = tf.train.latest_checkpoint(saver_folder)
        if self.restore_model and ckpt:
            saver.restore(sess, save_path=ckpt)
            print('Restored models from ', ckpt)
        else:
            print('No model restore, starting from scratch.')
            os.makedirs(saver_folder)

        # update number of epochs
        if self.restore_model:  # if restoring model then self.global_epoch has the last finished epoch so need to start from one after
            sess.run(increment_global_epochs)
        epoch_start = self.global_epochs.eval(sess)
        print('Starting from epoch ', epoch_start)

        for epoch_id in range(epoch_start, epoch_start + num_epochs):
            self.train_epoch(sess, epoch_id, writer)

            # save model
            if epoch_id % save_every == 0 or epoch_id == num_epochs - 1:
                check_path = saver.save(sess, save_path=saver_folder + '/model', global_step=epoch_id)
                print('Saved model params to {}'.format(check_path))

            # increment counter for next restore
            sess.run(increment_global_epochs)

        writer.close()
        sess.close()

    def test_model(self, plt_reconstruct=True, plt_generations=True):
        """Test existing model over test data"""
        # dropout operates differently at train and test
        self.train_mode = False
        test_eps = {self.eps_mult: 0, self.z_resample: 1}

        self.restore_model = True  # make sure it tries to restore
        saver = tf.train.Saver()
        saver_folder = self.get_saver_folder()
        sess = tf.Session()

        # restore model
        ckpt = tf.train.latest_checkpoint(saver_folder)
        if ckpt:
            saver.restore(sess, save_path=ckpt)
            print('Restored models from ', ckpt)
        else:
            print('No model to restore and test over.')
            return None

        def _plot_reconstructions(orig, rec):
            x_orig = orig[:10].squeeze()
            x_orig = np.moveaxis(x_orig, 0, 1)
            x_orig = np.reshape(x_orig, [self.img_size, self.img_size * 10])
            x_reconstruct = rec[:10].squeeze()
            x_reconstruct = np.moveaxis(x_reconstruct, 0, 1)
            x_reconstruct = np.reshape(x_reconstruct, [self.img_size, self.img_size * 10])

            plt_orig = tf.summary.image('orig', x_orig[np.newaxis,:,:,np.newaxis], max_outputs=10)
            plt_rec = tf.summary.image('reconstruct', x_reconstruct[np.newaxis, :, :, np.newaxis], max_outputs=10)

            img_orig, img_rec = sess.run((plt_orig, plt_rec))
            writer.add_summary(img_orig)
            writer.add_summary(img_rec)

        def _plot_generations():
            z_prior = np.random.normal(size=(10, self.latent_dim))

            x_gen = sess.run(self.x_out, feed_dict={self.z: z_prior, self.z_resample: 1})
            x_gen = x_gen.squeeze()
            x_gen = np.moveaxis(x_gen, 0, 1)
            x_gen = np.reshape(x_gen, [self.img_size, self.img_size * 10])

            plt_gen = tf.summary.image('generate', x_gen[np.newaxis, :, :, np.newaxis], max_outputs=10)

            img_gen = sess.run(plt_gen)
            writer.add_summary(img_gen)

        total_loss = 0
        num_batches = 0

        writer_folder = self.get_writer_folder()
        writer = tf.summary.FileWriter(writer_folder)
        sess.run(self.initializers['test'])
        while True:
            try:
                if plt_reconstruct:
                    step_loss, x_out, x_in = sess.run((self.loss_vae, self.x_out, self.x_batch), feed_dict=test_eps)
                    _plot_reconstructions(x_in, x_out)
                    plt_reconstruct = False
                else:
                    step_loss = sess.run(self.loss_vae, feed_dict=test_eps)
                num_batches += 1
                total_loss += step_loss
            except tf.errors.OutOfRangeError:
                break

        avg_test_loss = tf.strings.format("Avg test loss {} after {} epochs", (tf.constant(total_loss / num_batches), self.global_epochs))
        tb_test_loss = tf.summary.text('Averag test loss', avg_test_loss)
        writer.add_summary(sess.run(tb_test_loss))
        print(
            "Avg test loss {:.4f} after {:d} epochs".format((total_loss / num_batches), self.global_epochs.eval(sess)))

        if plt_generations:
            _plot_generations()

        writer.close()
        sess.close()


if __name__ == '__main__':
    NUM_EPOCHS = 20
    RESTORE_MODEL = False
    SAVE_EVERY = 5
    DATA_FUNC = loaders.load_mnist

    # instantiate network object and build computational graph
    ffnet = VaeDense(restore_model=RESTORE_MODEL, data_load_func=DATA_FUNC)

    # you can train or not a model
    ffnet.train_model(num_epochs=NUM_EPOCHS, save_every=SAVE_EVERY)

    # you can test or not the model (won't do anything if a previous model is not stored)
    ffnet.test_model(plt_generations=True, plt_reconstruct=True)


