# MLP
[mlp.py](mlp.py) is a simple multilayer perceptron (feed forward net) with 2 hidden layers and dropout on each.


### Graph building
There are two versions of the graph construction

* low level API using `tf.nn` in `FeedForwardNet.build_graph_base()`
* higher level API using `tf.layers` in `FeedForwardNet.build_graph()`

I did this simply as an exercise to learn both approaches.
I did not really check extensively but it seems that the `tf.layers` is somewhat faster.

The graph is build every time the net object is instantiated (within `__init__`).

There is a wrapper method `FeedForwardNet.build_model()` which makes sure that all parts of the model are constructed.

### Training and testing
Training and testing are implemented as methods of the object.

* `FeedForwardNet.train_model()` over the train data, including checking accuracy over validation set at every epoch
* `FeedForwardNet.test_model()` over test data

### Data handling
I use tf.data API to manage the looping over batches and epochs.

The dataset construction is done in the `loaders` helper module. I only created `loaders.load_mnist()` so far.

The iterators are constructed in `FeedForwardNet.read_data()`.

### Saving model checkpoints
Using `tf.train.Saver` to save and restore from model checkpoints.
Saving only model parameters not the whole model.
So model still needs to be build by instantiating an object of the class.

### Printing model graph and traing progress to tensorboard
Using `tf.summary` to store the model graph and the training progress.
The train step counts are updated automatically from the optimizer through `global_step`.

I create separate folders for train and val training progress so that I can use the same summary variables which, however, get written to different files for the different regimes.

### Performance
With the default settings, classification of MNIST after 20 epochs is about 97.5% on test data.

### To improve

* The **saver folder names** are very crude and don't include all the possible user settings pertaining to the specific model run.
Changing the number of hidden layers and trying to restore will result in error as the method does not make sure that what you restore is compatible with what you want to run.
Should be relatively easy to fix by making the naming convention for the saver folder more complex.

* For now I delete the old **tb folders** for every new run. This means that on a restore, the old progress is lost.
Not sure if I kept the old files, tensorboard could merge these automatically. Or if I can do it somehow programmatically?

* The `__init__` has a long list initiating the class attributes. Do I need all of them? (Basically all the methods output `self.attrib` instead of just returning something.)
Is there a better way to deal with this?

### Things to remember

* **Dropout** needs to operate differently in train and test regimes.
This was rather fiddly in the lower-level API.
In the higher level `tf.layers.dropout` there is a flag for the train vs test regimes so this simply needs to be passed in as a parameter and set up correctly in the train and test regimes.
It displays somewhat strangely in tensorboard with the first dropout operation being connected to all later dropouts. This is not because it passes some data but because the API creates a placeholder within the 1st dropout layer to indicate if this is in the train or test regime.

* It seems important to `tf.reset_default_graph()` every time I want to build the new graph to avoid GPU memory problems.
I think it frees the memory which otherwise is not the case (not even after closing the `tf.Session`).
I have this as the 1st step of building the graph in `__init__` of the class.


