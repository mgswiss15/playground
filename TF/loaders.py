""" Standard data loaders

    Magda, 26/12/2018
"""

import tensorflow as tf
import numpy as np

def load_mnist(batch_size=128):
    """Load MNIST data from keras."""
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

    num_classes = len(np.unique(y_train))
    img_size = x_train.shape[1]
    channels = 1

    # split train to train and validation
    x_val = x_train[-1000:]
    x_train = x_train[:-1000]
    y_val = y_train[-1000:]
    y_train = y_train[:-1000]

    def _preprocess_data(x, y):
        """Normalize colors to [0,1], expand x dims for 1 channel, one-hot encode labels."""
        x /= 255
        return (
            tf.expand_dims(x, -1),
            tf.one_hot(y, num_classes)
        )

    # create train/val/test datasets
    train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
    train_dataset = train_dataset.shuffle(buffer_size=len(y_train)).batch(batch_size)
    train_dataset = train_dataset.map(_preprocess_data)

    val_dataset = tf.data.Dataset.from_tensor_slices((x_val, y_val))
    val_dataset = val_dataset.batch(len(y_val))
    val_dataset = val_dataset.map(_preprocess_data)

    test_dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
    test_dataset = test_dataset.batch(1000)
    test_dataset = test_dataset.map(_preprocess_data)

    return (
        train_dataset,
        val_dataset,
        test_dataset,
        (img_size, num_classes, channels)
    )
