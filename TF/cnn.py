""" Convolutional classification network

    Magda, 26/12/2018
"""

import tensorflow as tf
import numpy as np
import time
import os, shutil
import loaders


class ConvNet(object):
    """CNN for classification.

    Two hidden layers network with dropout in both hidden layers.
    """

    def __init__(self, restore_model=True, batch_size=64, data_load_func=None, channels_1=32,
                 channels_2=64, learn_rate=0.001, keep_prob=0.8):
        # training parameters
        self.restore_model = restore_model
        self.batch_size = batch_size
        self.learn_rate = learn_rate
        # network structure
        self.channels_1 = channels_1
        self.channels_2 = channels_2
        self.keep_prob = keep_prob
        self.train_mode = None
        # data
        self.data_load_func = data_load_func
        self.train_dataset = None
        self.val_dataset = None
        self.test_dataset = None
        self.img_size = None
        self.num_classes = None
        self.channels = None
        self.x_batch = None
        self.y_batch = None
        self.initializers = None
        # graph variables
        self.logits = None
        self.loss = None
        self.train_optim = None
        self.accuracy = None
        self.tb_summaries = None
        # build graph
        tf.reset_default_graph()
        self.gph = tf.get_default_graph()
        self.global_step = tf.Variable(initial_value=0, name='gstep', trainable=False, dtype=tf.int32)
        self.global_epochs = tf.Variable(initial_value=1, name='gepoch', trainable=False, dtype=tf.int32)
        self.build_model()

    def read_data(self):
        """Use load function to get data and prepare tensorflow dataset and iterators"""
        with tf.name_scope('data'):
            # load train/val/test data
            self.train_dataset, self.val_dataset, self.test_dataset, data_params = self.data_load_func(
                batch_size=self.batch_size)
            self.img_size = data_params[0]
            self.num_classes = data_params[1]
            self.channels = data_params[2]

            # create iterators
            iterator = tf.data.Iterator.from_structure(output_types=self.train_dataset.output_types,
                                                       output_shapes=self.train_dataset.output_shapes)
            self.x_batch, self.y_batch = iterator.get_next()

            self.initializers = {
                'train': iterator.make_initializer(self.train_dataset),  # initializer for train data
                'val': iterator.make_initializer(self.val_dataset),  # initializer for val data
                'test': iterator.make_initializer(self.test_dataset),  # initializer for test data
            }

    def build_graph(self):
        """Tow hidden-layers convnet using higher-level API"""
        conv1 = tf.layers.conv2d(self.x_batch, filters=self.channels_1, kernel_size=3, strides=1, padding='same',
                                 use_bias=True, activation=tf.nn.relu, kernel_initializer=tf.glorot_normal_initializer,
                                 name='conv1')
        conv1 = tf.layers.dropout(conv1, rate=self.keep_prob, training=self.train_mode, name='drop_c1')
        conv2 = tf.layers.conv2d(conv1, filters=self.channels_2, kernel_size=3, strides=1, padding='same',
                                 use_bias=True, activation=tf.nn.relu, kernel_initializer=tf.glorot_normal_initializer,
                                 name='conv2')
        conv2 = tf.layers.dropout(conv2, rate=self.keep_prob, training=self.train_mode, name='drop_c2')
        conv2 = tf.layers.flatten(conv2, name='flat_c2')
        self.logits = tf.layers.dense(conv2, units=self.num_classes, use_bias=True,
                                      kernel_initializer=tf.glorot_normal_initializer, name='out')

    def get_loss(self):
        """Cross entropy loss (for training)"""
        with tf.name_scope('loss'):
            cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=self.y_batch, logits=self.logits,
                                                                       name='cross_entropy')
            self.loss = tf.reduce_mean(cross_entropy, name='loss')

    def get_accuracy(self):
        """Accuracy for model evaluation"""
        with tf.name_scope('accuracy'):
            matches = tf.equal(tf.argmax(self.logits, axis=1), tf.argmax(self.y_batch, axis=1), name='matches')
            self.accuracy = tf.reduce_mean(tf.to_float(matches), name='accuracy')

    def get_summaries(self):
        """Sumaries for tensorboard plotting"""
        self.tb_summaries = {
            'loss': tf.summary.scalar('loss', self.loss),
            'acc': tf.summary.scalar('accuracy', self.accuracy)
        }

    def create_optimizer(self):
        optim = tf.train.AdamOptimizer(learning_rate=self.learn_rate, name='optim')
        self.train_optim = optim.minimize(self.loss, global_step=self.global_step)

    def build_model(self):
        """Build complete inference model"""
        self.read_data()
        self.build_graph()
        self.get_loss()
        self.get_accuracy()
        self.get_summaries()
        self.create_optimizer()

    def train_epoch(self, sess, epoch_id, writer):
        """Run one training epoch"""
        # dropout operates differently at train and test
        self.train_mode = True

        start_time = time.time()
        total_loss = 0
        train_accuracy = 0
        num_batches = 0

        sess.run(self.initializers['train'])
        while True:
            try:
                _, step_loss, step_accuracy = sess.run((self.train_optim, self.loss, self.accuracy))
                num_batches += 1
                total_loss += step_loss
                train_accuracy += step_accuracy

                # tb plotting
                step, tb_loss, tb_acc = sess.run(
                    (self.global_step, self.tb_summaries['loss'], self.tb_summaries['acc']))
                writer.add_summary(tb_loss, global_step=step)
                writer.add_summary(tb_acc, global_step=step)
            except tf.errors.OutOfRangeError:
                break

        print('Epoch {:d} finished in {:.4f} over {:d} batches, avg loss: {:.4f}, avg train accruacy: {:.4f}'.format(
            epoch_id, time.time() - start_time, num_batches, total_loss / num_batches, train_accuracy / num_batches))

    def validate(self, sess, writer):
        """One evaluation over whole validation set"""
        # dropout operates differently at train and test
        self.train_mode = False

        sess.run(self.initializers['val'])
        step_accuracy, tb_acc, step = sess.run((self.accuracy, self.tb_summaries['acc'], self.global_step))
        print('Avg val accuracy: {:4f}'.format(step_accuracy))
        writer.add_summary(tb_acc, global_step=step)

    def get_saver_folder(self):
        """Where model checkpoints are stored based on param configs"""
        saver_folder = './checkpoints/cnn/lr_' + str(self.learn_rate) + '_kp_' + str(self.keep_prob)

        if not self.restore_model and os.path.exists(saver_folder):
            if os.path.exists(saver_folder + '_old'):
                shutil.rmtree(saver_folder + '_old')
            shutil.move(saver_folder, saver_folder + '_old')
            print('Moved old checkpoints to ', saver_folder + '_old')

        return saver_folder

    def get_writer_folder(self):
        """Where tensorboard files are stored based on param configs"""
        writer_train_folder = './tb_graphs/cnn/train/lr_' + str(self.learn_rate) + '_kp_' + str(self.keep_prob)
        writer_val_folder = './tb_graphs/cnn/val/lr_' + str(self.learn_rate) + '_kp_' + str(self.keep_prob)

        # clean old folders
        if os.path.exists(writer_train_folder):
            if os.path.exists(writer_train_folder + '_old'):
                shutil.rmtree(writer_train_folder + '_old')
            shutil.move(writer_train_folder, writer_train_folder + '_old')
        if os.path.exists(writer_val_folder):
            if os.path.exists(writer_val_folder + '_old'):
                shutil.rmtree(writer_val_folder + '_old')
            shutil.move(writer_val_folder, writer_val_folder + '_old')

        return writer_train_folder, writer_val_folder

    def train_model(self, num_epochs, save_every):
        var_init = tf.global_variables_initializer()
        saver = tf.train.Saver()
        saver_folder = self.get_saver_folder()
        increment_global_epochs = tf.assign(self.global_epochs, self.global_epochs + 1)

        writer_train_folder, writer_val_folder = self.get_writer_folder()
        writer_train = tf.summary.FileWriter(writer_train_folder, self.gph)
        writer_val = tf.summary.FileWriter(writer_val_folder)

        # main session
        sess = tf.Session()
        sess.run(var_init)

        # restore model from previous checkpoints
        ckpt = tf.train.latest_checkpoint(saver_folder)
        if self.restore_model and ckpt:
            saver.restore(sess, save_path=ckpt)
            print('Restored models from ', ckpt)
        else:
            print('No model restore, starting from scratch.')
            os.makedirs(saver_folder)

        # update number of epochs
        epoch_start = self.global_epochs.eval(sess)
        print('Starting from epoch ', epoch_start)

        for epoch_id in range(epoch_start, epoch_start + num_epochs):
            self.train_epoch(sess, epoch_id, writer_train)
            self.validate(sess, writer_val)

            # increment counter for next restore
            global_epoch = sess.run(increment_global_epochs)

            # save model
            if epoch_id % save_every == 0 or epoch_id == num_epochs - 1:
                check_path = saver.save(sess, save_path=saver_folder + '/model', global_step=epoch_id)
                print('Saved model params to {}'.format(check_path))

        sess.close()

    def test_model(self):
        """Test existing model over test data"""
        # dropout operates differently at train and test
        self.train_mode = False

        self.restore_model = True  # make sure it tries to restore
        saver = tf.train.Saver()
        saver_folder = self.get_saver_folder()
        sess = tf.Session()

        # restore model
        ckpt = tf.train.latest_checkpoint(saver_folder)
        if ckpt:
            saver.restore(sess, save_path=ckpt)
            print('Restored models from ', ckpt)
        else:
            print('No model to restore and test over.')
            return None

        test_accuracy = 0
        num_batches = 0

        sess.run(self.initializers['test'])
        while True:
            try:
                step_accuracy = sess.run(self.accuracy)
                test_accuracy += step_accuracy
                num_batches += 1
            except tf.errors.OutOfRangeError:
                break

        print('Avg test accuracy: {:4f}'.format(test_accuracy/num_batches))


if __name__ == '__main__':
    NUM_EPOCHS = 10
    RESTORE_MODEL = False
    SAVE_EVERY = 5
    DATA_FUNC = loaders.load_mnist

    # instantiate network object
    ffnet = ConvNet(restore_model=RESTORE_MODEL, data_load_func=DATA_FUNC)

    # you can train or not a model
    ffnet.train_model(num_epochs=NUM_EPOCHS, save_every=SAVE_EVERY)

    # you can test or not the model (won't do anything if a previous model is not stored)
    ffnet.test_model()
