# VAE
[vae_dense.py](vae_dense.py) is a dense (feed forward) variational autoencoder.
The implementation follows the thechnical notes [TechNotes.pdf](https://www.dropbox.com/s/vllgyge3uldgzls/techNotes.pdf?dl=0)

### Data handling
Uses tf.data API to manage the looping over batches and epochs.

The dataset construction is done in the `loaders` helper module.

The iterators are constructed in `read_data()`.

### Graph building
Using the `tf.layers` API.

Done in several steps which are all bundled within `build_model()`:

* `read_data()`
* `build_encoder()`
* `get_latent()` - this is somewhat uggly cause needs to cater for training / testing and generations (see below)
* `build_decoder()`
* `get_loss` - negative ELBO, I'm assuming gaussian distributions so the cross-entropy term boils down to squared error loss
* `get_summaries()` - scalars for tensorboard
* `create_optimizer()` - standard ADAM
 
`build_model()` is called from `__init__` so that the computational graph is build at every istantiation of the model object.

### Training and testing
Training and testing are implemented as methods of the object.

* `train_model()` - uses `train_epoch()` to iterate over the epochs, saves model checkpoints and prints summary scalars with loss for tensorboard.
In the very end I added z resampling (Jason mentioned it) but though I can see that theoretically it may help, on the MNIST dataset the effect is not great.
Moreover it is a stochastic gradient descent with minibatches so not sure how important this really is.
It can be set to 1 and then is just standard single sample z. 
 
* `test_model()` - passes test set through the trained model (recovers the latets one from checkpoints),
gets the reconstructed images, calculates the negative ELBO over these (prints to tensorbaord text), optionally plots a few reconstruction and generation examples to tensorboard. 

Bringing the testing into the model within a single computational graph was a bit tricky.

*First*, for test time reconstructions the latent variable *z* is not sampled from the posterior (the reparametrization trick) but simply takes the posterior mean value.
I achieve this within `get_latent()` via `eps_mult` which is 1 at training but I feed it in as zeros at test time.
Not the neatest solution, but I can't think of anything better.

*Second*, for generations the latent *z* is not sampled from the posterior but from the prior.
I do this by feeding samples from the prior to `self.z` when running the session to get the x outputs.
Seems to work but seems hacky. Not sure if this is the way people do it.  

### Saving model checkpoints
Using `tf.train.Saver` to save and restore from model checkpoints.
Saving only model parameters not the whole model.
So model still needs to be build by instantiating an object of the class.

I create the save folder names to have all details of the model user parameters so I have a trace of multiple models. 

### Printing model graph and traing progress to tensorboard
Using `tf.summary` to store the model graph and the training progress.
The train step counts are updated automatically from the optimizer through `global_step`.

I create the writer folder names to have all details of the model user parameters so I have a trace of multiple models. 

### Performance
With the default settings, negative test ELBO after 20 epochs is around 29 (whatever that means).

### To improve

* The `__init__` has a long list initiating the class attributes. Do I need all of them? (Basically all the methods output `self.attrib` instead of just returning something.)
Is there a better way to deal with this?

### Things to remember

* Handling of the latent varible for the different regimes (train / test-reconstruct / test-generate) seems a bit messy but works so perhaps just fine.