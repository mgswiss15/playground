# Classification VAE
[cvae_dense.py](cvae_dense.py) is a dense (feed forward) variational autoencoder with the extra classification structure suggested by Frantzeska.

The netowrk implementation follows from [vae.md](vae.md) adding the classification (soft-max cross entropy) used in [mlp.md](mlp.md).

I'm using the same decoder network to spit out the *x* reconstructions and the *y* classes. 

### Training and testing
Same as in vae only adding the train / validation / test accuracy in the same way as in the mlp code.

### Performance
With the default settings, negative test ELBO after 100 epochs is around 27 (whatever that means) and the classification accuracy is around 94%.

