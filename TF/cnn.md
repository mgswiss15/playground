# CNN
[cnn.py](cnn.py) is a simple convolutional net with 2 hidden layers and dropout for each.

Only using the `tf.layers` API for simplicity. I had the `tf.nn` implementation at a point, not more difficult then feed forward so no point keep doing this.

Otherwise the structure follows closely the MLP implementation above including all its problems listed in the **To improve** section.  

### Performance
With the default settings, classification of MNIST after 10 epochs is about 98.5% on test data.

