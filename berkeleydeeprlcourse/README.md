# Deep Reinforcement Learning

## HW assignment

Homework assignments from the UC Berkeley course **CS294-112**
given by *Sergey Levine*.

Each of the hw folders has full assignment, including course provided code
and the hw assignment pdf.

My solution files begin with `mg_`.
