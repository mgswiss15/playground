#!/bin/bash
#set -eux
# for e in Hopper-v2 Ant-v2 HalfCheetah-v2 Humanoid-v2 Reacher-v2 Walker2d-v2
for e in Humanoid-v2
do
    python mg_bc.py expert_data $e --num_rollouts=200 --num_epochs=100
done
