#!/bin/bash
#set -eux
python mg_run_policy.py HalfCheetah-v2 --num_rollouts=20 --num_epochs=50 --saver_dir=./saver_dagger --writer_dir=./tb_dagger --num_episodes=100 --print_reward
python mg_run_policy.py HalfCheetah-v2 --num_rollouts=100 --num_epochs=100 --saver_dir=./saver_bc --writer_dir=./tb_bc --num_episodes=100 --print_reward
