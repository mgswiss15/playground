"""
MG 31/1/2019: HW1 train dagger
"""
import pickle
import tensorflow as tf
import argparse
import os
import shutil
import numpy as np
import load_policy
import mg_bc
import mg_run_policy


def load_expert_policy(envname):
    expert_policy_file = os.path.join('./experts', envname + '.pkl')
    print('loading and building expert policy')
    policy_fn = load_policy.load_policy(expert_policy_file)
    print('loaded and built')
    return policy_fn


def augment_expert_data(envname, num_rollouts, obs_add, act_add):
    expert_data_file = os.path.join('./expert_dagger', 'R' + str(num_rollouts),
                                    envname + '.pkl')
    print("Loading {} expert data.".format(envname))
    with open(expert_data_file, 'rb') as f:
        expert_data = pickle.load(f)

    obs = expert_data['observations'].astype(np.float32)
    print('Expert data have now size')
    print(obs.shape)
    act = expert_data['actions'].astype(np.float32)

    expert_data = {'observations': np.concatenate((obs, obs_add), axis=0),
                   'actions': np.concatenate((act, act_add), axis=0)}

    with open(expert_data_file, 'wb') as f:
        pickle.dump(expert_data, f, pickle.HIGHEST_PROTOCOL)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('envname', type=str)
    parser.add_argument('--num_rollouts', type=int, default=20)
    parser.add_argument('--learning_rate', type=int, default=0.0001)
    parser.add_argument('--batch_size', type=int, default=128)
    parser.add_argument('--num_epochs', type=int, default=100)
    parser.add_argument('--render', action='store_true')
    parser.add_argument('--dagger_iter', type=int, default=5)
    parser.add_argument('--just_data_size', action='store_true')
    args = parser.parse_args()

    SAVER_DIR = './saver_dagger'

    # load_expert_policy(args.envname)
    tf.reset_default_graph()
    gph = tf.get_default_graph()
    policy_fun = load_expert_policy(args.envname)

    if args.just_data_size:
        e_file = os.path.join('./expert_dagger', 'R' + str(args.num_rollouts),
                              args.envname + '.pkl')
        print("Loading {} expert data.".format(args.envname))
        with open(e_file, 'rb') as f:
            expert_data = pickle.load(f)

        obs = expert_data['observations'].astype(np.float32)
        print('Expert data have now size')
        print(obs.shape)
        return

    # make sure the expert data exists
    expert_dir = os.path.join('./expert_dagger', 'R' + str(args.num_rollouts))
    if not os.path.exists(expert_dir):
        os.makedirs(expert_dir)
    epxert_data_file = os.path.join(expert_dir, args.envname + '.pkl')
    if not os.path.exists(epxert_data_file):
        orig_file = os.path.join('./expert_data', 'R' + str(args.num_rollouts),
                                 args.envname + '.pkl')
        shutil.copy(orig_file, epxert_data_file)

    for dag_iter in range(args.dagger_iter):
        # train imitation policy over expert dataset
        WRITER_DIR = './tb_dagger'
        mg_bc.main(['expert_dagger', args.envname,
                    '--num_rollouts=' + str(args.num_rollouts),
                    '--num_epochs=' + str(args.num_epochs),
                    '--saver_dir=' + SAVER_DIR,
                    '--writer_dir=' + WRITER_DIR,
                    '--saver_restore'])

        # run imitation policy to collect observations
        if args.render:
            args_run_policy = ([args.envname,
                                '--num_rollouts=' + str(args.num_rollouts),
                                '--num_epochs=' + str(args.num_epochs),
                                '--saver_dir=' + SAVER_DIR,
                                '--writer_dir=' + WRITER_DIR, '--render',
                                '--policy_run_num=' + str(dag_iter),
                                '--print_reward'])
        else:
            args_run_policy = ([args.envname,
                                '--num_rollouts=' + str(args.num_rollouts),
                                '--num_epochs=' + str(args.num_epochs),
                                '--saver_dir=' + SAVER_DIR,
                                '--writer_dir=' +  WRITER_DIR,
                                '--policy_run_num=' + str(dag_iter),
                                '--print_reward'])

        pol_out = mg_run_policy.main(args_run_policy)
        observations, _, _ = pol_out

        # run expert policy over new observations
        # sess_config = tf.ConfigProto(log_device_placement=True,
        #                              device_count={'GPU': 0})
        sess_config = tf.ConfigProto(device_count={'GPU': 0})
        with tf.Session(graph=gph,
                        config=sess_config):
            actions = policy_fun(observations)

        # add new expert data to data pool
        augment_expert_data(args.envname, args.num_rollouts, observations,
                            actions)


if __name__ == '__main__':
    main()
