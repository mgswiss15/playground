# CS294-112 HW 1: Imitation Learning

## CS294-112 course instructions

Dependencies:
 * Python **3.5**
 * Numpy version **1.14.5**
 * TensorFlow version **1.10.5**
 * MuJoCo version **1.50** and mujoco-py **1.50.1.56**
 * OpenAI Gym version **0.10.5**

Once Python **3.5** is installed, you can install the remaining dependencies using `pip install -r requirements.txt`.

**Note**: MuJoCo versions until 1.5 do not support NVMe disks therefore won't be compatible with recent Mac machines.
There is a request for OpenAI to support it that can be followed [here](https://github.com/openai/gym/issues/638).

**Note**: Students enrolled in the course will receive an email with their MuJoCo activation key. Please do **not** share this key.

The only file that you need to look at is `run_expert.py`, which is code to load up an expert policy, run a specified number of roll-outs, and save out data.

In `experts/`, the provided expert policies are:
* Ant-v2.pkl
* HalfCheetah-v2.pkl
* Hopper-v2.pkl
* Humanoid-v2.pkl
* Reacher-v2.pkl
* Walker2d-v2.pkl

The name of the pickle file corresponds to the name of the gym environment.

## MG comments on solution

Tensorflow implementation of

- behavioral cloning [mg_bc.py](mg_bc.py)
- dagger [mg_dagger.py](mg_dagger.py)
- interaction with open ai mujoco environments [mg_run_policy.py](mg_run_policy.py)

### Usage

For both methods you first need to generate roll-outs from the expert policy by `run_expert.py`.
`mg_roll_expert.bash` has the appropriate python calls.
For bc reasonable numbers are 100 to 200 roll-outs, for dagger I use 20.
This will create `expert_data` dir and store the `observation-actions` roll-out data within.

Training policies:

- bc: `mg_train_bc.bash` calls `mg_bc.py`
- dagger: `mg_train_dagger.bash` calls `mg_dagger.py` which in turn relies on `mg_bc.py` and `mg_run_policy.py`.
Dagger also copies the original `expert_data` into `expert_dagger` dir and augments the expert policy roll-outs with the augmentation observation(current policy) - action(expert) data.
I trained the dagger starting with 20 expert policy roll-outs and then using 20 current policy roll-outs for querying the expert before retraining. 
The number of dagger iterations is a parameter to the method. I used 5 to begin with and if it was not enough, I continued training in 5 iterations increments (manually, easier then coming up with a sophisticated automated solution).

Both implementations save the learned models to saver directories and print out the training loss to tensorboard.
Dagger also tries to print the total reward par dagger iteration but this does not work that great (would need some cleaning but hot worth the time/effort so left as is).


### Results

Final models of the two methods are in `saver_bc` and `saver_dagger`.
The respective tensorboard prints of the total reward generated over 100 roll-outs of the policy are in `tb_bc` and `tb_dagger`.

Total reward averaged across 100 roll-outs of final trained policy.
Queries indicates the total number of the environment queries.
This corresponds to number of observations in the training data. 
For bc this is simply the number of observations per roll-out times number of rollouts (e.g. 1000*100 = 100k).
For dagger this depends on the number of dagger iterations and the lenght of intermidate policy roll-outs (often early finish).

| Environment | expert mean/std reward| bc mean/std/queries reward | dagger mean/std/queries reward |
|---|---|---|---|
|Humanoid-v2| ~10k | 264 / 25 / 200k | 2686 / 2128 / 328k  |
|Ant-v2| ~4500 | 4173 / 480 / 200k | 4340 / 560 / 118k |
|Walker-v2 | ~5500 | 863 / 306 / 200k | 4294 / 1341 / 218k |
|Hopper-v2 | ~3700 | 166 / 1.4 / 200k | 3541 / 400/ 255k |
|HelfCheetah-v2 | ~4100 | 3297 / 197 / 100k | 3525 / 510 / 120k | 

To render the final dagger policies in mujoco environment run the following (change the environment name as you wish)

`python mg_run_policy.py HalfCheetah-v2 --num_rollouts=20 --num_epochs=50 --saver_dir=./saver_dagger --num_episodes=10 --render`








