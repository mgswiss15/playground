""" testing gym and mujoco
MG 23/1/2019
"""

import gym

env = gym.make('Ant-v2')
print('Action space')
print(env.action_space.high)
print(env.action_space.low)
for i_episode in range(10):
    observation = env.reset()
    for t in range(1000):
        env.render()
        print(observation)
        action = env.action_space.sample()
        print('Action at step {}'.format(t + 1))
        print(action)
        print(action.shape)
        observation, reward, done, info = env.step(action)
        if done:
            print('Episode finished after {} timestamps'.format(t + 1))
            break
env.close()
