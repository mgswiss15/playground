"""
MG 29/1/2019: HW1 train behavioral cloning policy
"""

import pickle
import tensorflow as tf
import argparse
import os
import shutil
import numpy as np
import time


def load_policy_rollout(policy_rollout_dir, envname, num_rollouts, batch_size):
    rollout_fname = os.path.join(policy_rollout_dir, 'R' + str(num_rollouts),
                                 envname + ".pkl")
    print("Loading {} policy rollout data.".format(envname))
    with open(rollout_fname, 'rb') as f:
        rollout_data = pickle.load(f)

    observations = rollout_data['observations'].astype(np.float32)
    obs_len, obs_dim = observations.shape
    actions = rollout_data['actions'].astype(np.float32)
    act_dim = actions.shape[1]

    train_set = tf.data.Dataset.from_tensor_slices((observations, actions))
    train_set = (train_set.shuffle(buffer_size=obs_len).batch(batch_size))
    return train_set, obs_dim, act_dim


def create_iterators(dataset):
    iterator = dataset.make_initializable_iterator()
    next_batch = iterator.get_next()
    return iterator, next_batch


def build_net(obs, obs_dim, act_dim):
    """Build 2 layer feed forward net with drop-out"""
    train_mode = tf.placeholder(dtype=tf.bool, name='train_mode')
    h1 = tf.layers.dense(obs, units=64, use_bias=True,
                         kernel_initializer=tf.glorot_normal_initializer,
                         activation=tf.nn.relu, name='h1')
    h1 = tf.layers.dropout(h1, rate=0.5, training=train_mode, name='drop_h1')
    h2 = tf.layers.dense(h1, units=64, use_bias=True,
                         kernel_initializer=tf.glorot_normal_initializer,
                         activation=tf.nn.relu, name='h2')
    h2 = tf.layers.dropout(h2, rate=0.5, training=train_mode, name='drop_h2')
    acts = tf.layers.dense(h2, units=act_dim, use_bias=True,
                           kernel_initializer=tf.glorot_normal_initializer,
                           activation=tf.tanh, name='acts')
    return acts, train_mode


def get_loss(acts_true, acts_pred):
    with tf.name_scope('loss'):
        loss = tf.reduce_mean(tf.squared_difference(acts_true, acts_pred))
    return loss


def create_optimizer(loss, lr, gstep):
    optim = tf.train.AdamOptimizer(learning_rate=lr)
    train_optim = optim.minimize(loss=loss, global_step=gstep)
    return train_optim


def create_summaries(loss):
    """Summaries for tensorboard plotting"""
    tb_loss = tf.summary.scalar('loss', loss)
    return tb_loss


def build_model(batch, obs_dim, act_dim, learning_rate):
    global_step = tf.Variable(initial_value=0, name='gstep',
                              trainable=False, dtype=tf.int32)
    acts_pred, train_mode = build_net(batch[0], obs_dim, act_dim)
    loss = get_loss(batch[1], acts_pred)
    train_optim = create_optimizer(loss, learning_rate, global_step)
    tb_loss = create_summaries(loss)
    return acts_pred, loss, tb_loss, train_mode, train_optim, global_step


def train_epoch(sess, model_vars, writer, epoch):
    """Train single epoch"""
    _, loss, tb_loss, train_mode, train_optim, global_step = model_vars
    start_time = time.time()
    total_loss = 0
    num_batches = 0

    while True:
        try:
            _, batch_loss = sess.run((train_optim, loss),
                                     feed_dict={train_mode: True})
            num_batches += 1
            total_loss += batch_loss

            writer_loss, gstep = sess.run((tb_loss, global_step),
                                          feed_dict={train_mode: True})
            writer.add_summary(writer_loss, gstep)
        except tf.errors.OutOfRangeError:
            break

    print('Epoch {:d} finished in {:.4f} over {:d} batches, \
          avg_loss={:.4f}'.format(epoch, time.time() - start_time,
                                  num_batches, total_loss))


def train_model(sess, model_vars, writer, iterator, saver_dir, num_epochs):
    """Train model over multiple epochs"""
    for epoch in range(num_epochs):
        sess.run(iterator.initializer)

        train_epoch(sess, model_vars, writer, epoch)

    saver = tf.train.Saver()
    save_file = saver.save(sess, save_path=os.path.join(saver_dir, 'model'))
    print('Model saved to {}'.format(save_file))


def main(pass_args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('policy_rollout_dir', type=str)
    parser.add_argument('envname', type=str)
    parser.add_argument('--num_rollouts', type=int, default=20)
    parser.add_argument('--learning_rate', type=float, default=0.0001)
    parser.add_argument('--batch_size', type=int, default=128)
    parser.add_argument('--num_epochs', type=int, default=300)
    parser.add_argument('--saver_dir', type=str, default='./saver_bc')
    parser.add_argument('--writer_dir', type=str, default='./tb_bc')
    parser.add_argument('--saver_restore', action='store_true')
    args = parser.parse_args(pass_args)

    tf.reset_default_graph()
    gph = tf.get_default_graph()

    policy_data = load_policy_rollout(args.policy_rollout_dir,
                                      args.envname, args.num_rollouts,
                                      args.batch_size)
    train_set, obs_dim, act_dim = policy_data

    iterator_specs = create_iterators(train_set)
    iterator, next_batch = iterator_specs

    model_vars = build_model(next_batch, obs_dim, act_dim, args.learning_rate)

    writer_dir = os.path.join(args.writer_dir, args.envname,
                              'NR' + str(args.num_rollouts) +
                              'LR' + str(args.learning_rate) +
                              'BS' + str(args.batch_size) +
                              'NE' + str(args.num_epochs))
    if os.path.exists(writer_dir):
        shutil.rmtree(writer_dir)
    writer = tf.summary.FileWriter(writer_dir, gph)

    saver_dir = os.path.join(args.saver_dir, args.envname,
                             'NR' + str(args.num_rollouts) +
                             'LR' + str(args.learning_rate) +
                             'BS' + str(args.batch_size) +
                             'NE' + str(args.num_epochs))

    if not os.path.exists(saver_dir):
        os.makedirs(saver_dir)

    # main session
    sess = tf.Session()
    model_restore = tf.train.latest_checkpoint(saver_dir)
    if args.saver_restore and model_restore:
        saver = tf.train.Saver()
        saver.restore(sess, save_path=model_restore)
    else:
        var_init = tf.global_variables_initializer()
        sess.run(var_init)

    train_model(sess, model_vars, writer, iterator, saver_dir, args.num_epochs)

    sess.close()
    writer.close()


if __name__ == '__main__':
    main()
