"""
MG 29/1/2019: run trained policy
"""

import tensorflow as tf
import argparse
import os
import gym
import numpy as np
import time


def main(pass_args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('envname', type=str)
    parser.add_argument('--num_rollouts', type=int, default=20)
    parser.add_argument('--learning_rate', type=int, default=0.0001)
    parser.add_argument('--batch_size', type=int, default=128)
    parser.add_argument('--num_epochs', type=int, default=300)
    parser.add_argument('--num_episodes', type=int, default=20)
    parser.add_argument('--saver_dir', type=str, default='./saver')
    parser.add_argument('--writer_dir', type=str, default='./tb_bc')
    parser.add_argument('--print_reward', action='store_true')
    parser.add_argument('--policy_run_num', type=int, default=0)
    parser.add_argument('--render', action='store_true')
    args = parser.parse_args(pass_args)

    tf.reset_default_graph()
    gph = tf.get_default_graph()

    saver_dir = os.path.join(args.saver_dir, args.envname,
                             'NR' + str(args.num_rollouts) +
                             'LR' + str(args.learning_rate) +
                             'BS' + str(args.batch_size) +
                             'NE' + str(args.num_epochs))

    writer_dir = os.path.join(args.writer_dir, args.envname,
                              'NR' + str(args.num_rollouts) +
                              'LR' + str(args.learning_rate) +
                              'BS' + str(args.batch_size) +
                              'NE' + str(args.num_epochs))

    saver = tf.train.import_meta_graph(os.path.join(saver_dir, 'model.meta'))

    sess_config = tf.ConfigProto(device_count={'GPU': 0})
    sess = tf.Session(config=sess_config)

    model_restore = tf.train.latest_checkpoint(saver_dir)
    saver.restore(sess, save_path=model_restore)

    writer = tf.summary.FileWriter(writer_dir)
    summary = tf.Summary()

    train_mode = gph.get_tensor_by_name('train_mode:0')
    acts_pred = gph.get_tensor_by_name('acts/Tanh:0')
    iterator = gph.get_tensor_by_name('IteratorGetNext:0')
    print('Restored model: ' + model_restore)

    env = gym.make(args.envname)
    observations = []
    actions = []
    rewards = []
    for episode in range(args.num_episodes):
        total_reward = 0
        observation = env.reset()

        for t in range(env.spec.timestep_limit):
            action = sess.run(acts_pred,
                              feed_dict={train_mode: False,
                                         iterator: observation[np.newaxis]})
            action = np.squeeze(action)
            actions.append(action)
            observation, reward, done, _ = env.step(action)
            observations.append(observation)
            total_reward += reward
            if args.render:
                env.render()
            if done:
                print('Episode {} finished after {} timesteps'.
                      format(episode + 1, t + 1))
                break

        rewards.append(total_reward)
        if args.print_reward:
            summary.value.add(tag='reward', simple_value=total_reward)
            writer.add_summary(summary,
                               (args.policy_run_num *
                                args.num_episodes) + episode)
        print('Episode {} total reward: {}'.format(episode + 1,
                                                   total_reward))

    env.close()
    writer.close()
    sess.close()
    print(rewards)
    print('Mean return per episode {}'.format(np.mean(rewards)))
    print('Std of return {}'.format(np.std(rewards)))
    return np.array(observations), np.array(actions), np.array(rewards)


if __name__ == '__main__':
    main()
