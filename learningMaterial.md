# Learning material
Links to useful learning material, some tips/tricks/recommendation

## Comp config
* **Notebook:** Dell XPS 15 9570
* **System:** Ubuntu 18.04
Installed using https://www.ubuntu.com/download/desktop - trivial
* **GPU** NVIDIA GeForce GTX 1000
Before installing the drivers you need to **disable secure boot in bios**!
For drivers I think (though not sure) I used ppa like here: https://askubuntu.com/questions/1054954/how-to-install-nvidia-driver-in-ubuntu-18-04
Watch out: you still need to install CUDA if you want to use it for ML computations. Be careful about versions here as not all cuda versions are compatible with all python software.
* **Python** version 3.7.0
Install instructions http://docs.anaconda.com/anaconda/install/ - trivial
* **Tensorflow for GPU** version 1.12.0
Installed via conda, e.g. like here https://towardsdatascience.com/tensorflow-gpu-installation-made-easy-use-conda-instead-of-pip-52e5249374bc
This installs a lower version of python (3.6.7) plus plenty of other stuff including **compatible cudatoolkig and cudnn**
* **interactive shell, IDE**
    * ipython, jupyter notebooks, spyder - depending on your conda package these are or are not included in the standard package bundle (I believe not included in the conda tensorflow_bundle so need to be installed later - trivial via conda)
    ipython is useful, jupyter notebooks good for teaching, haven't worked a lot with spyder
    * PyCharm - much more fancy IDE with plenty of helpers (code checking, completion, etc.) install from https://www.jetbrains.com/pycharm/download/#section=linux - trivial
* **RL specific**
    * OpenAI Gym - for install follow https://gym.openai.com/docs/#installation - trivial
    * MuJoCo - needs a license, student 1yr free from https://www.roboti.us/license.html - a couple days, easy

## Courses
List of courses I did or looked at with some comments

### Python programming
Obviously you can google plenty of resources not listed here. The obvious starting point is the official documentation.

#### Python basics
* Udemy *Complete Python Bootcamp*: good, for the basics of python objects etc.
* Udemy *Leaning Python for Data Analysis*: checked it out, no good for what I wanted, too much data analysis, too little python
* Udemy *Learn Python Programming Masterclass*: I only started this one before skipping to the Bootcamp. Didn't like it, too much focused on user interfacae

#### Tensorflow
* Udemy *Complete Guide to TensorFlow for Deep Learning with Python*: Not so clear, I was skipping bits and pieces and focused on the base Tensorflow rather than the APIs (totally skipping Estimator, Keras is probably useful but skipped that quite a bit).
* Standford *CS20 Tensorflow for Deep Learning Research*: good, no vidos but lecture notes and slides have snippets of code and explanations plus there are full programmes to put it all into action

### Neural networks in general
Again, plenty of resources (blogs, etc.) available online. I list here courses and tutorials I found the most useful.

* Standford *CS231n: Convolutional Neural Networks for Visual Recognition*: very good, given by Fei-Fei Li group.
Has videos, lecture notes and Assignments which are clearly described so possible to follow without lecturer feedback.
In the assignments you do feed forward, cnn, and rnn by numpy so including the backprop calculations.
Has a coding tutorial on PyTorch and Tensorflow.

### Implementing basic NNs in Tensorflow
I found these good to get you going when implementing the very 1st nets

* MLP and CNN: https://github.com/aymericdamien/TensorFlow-Examples
* VAE: the above + https://arxiv.org/abs/1606.05908

### RL
* David Silver's videos: http://www0.cs.ucl.ac.uk/staff/d.silver/web/Teaching.html – I watched them but didn't work for me. I don't remember if I don't write or do.
* Sutton's book – too verbose and not always relevant
* Berkeley CS294-112: Sergey Levine groups. Has videos and homeworks. So far I barely started the first homework cause had to get to speed with Tensorflow and basic networks first. Seems rather hardcore.

