# Playground
Learning python by trial and error

List of learning materials I found useful: [learningMaterial.md](learningMaterial.md) 

## TF folder
Tensorflow code for basic models such as mlp, cnn, vae.

See [TF/README.md](TF/RAEDME.md) for details.

## berkeleydeeprlcourse folder
Homework exercises of UC Berkeley CS294-112 course (Sergey Levine) 

See [berkeleydeeprlcourse/README.md](berkeleydeeprlcourse/RAEDME.md) for details.
